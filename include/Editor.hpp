//
//  Editor.hpp
//
//  This file contains the level editor
//
#ifndef EDITOR_HPP
#define EDITOR_HPP
#include <sstream>
#include <iostream>
#include <map>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>

#include "Screen.hpp"
#include "Settings.hpp"
#include "Map.hpp"
#include "EditorMenu.hpp"
#include "EditorTemporaryShape.hpp"

void addTheme(Map* map, int theme);

/**
 * Editor
 *
 * The main class of editor. This represents the level editor
 */
class Editor : public sf::Drawable, public Screen
{

private:

  sf::View gameView;           /// < Moving game field
  sf::View staticView;         /// < The part which will be static like points and speed
  sf::View skyView;            /// < Sky behind the game area which moves slower than the game area
  sf::View frontView;          /// < The area in front of the game area which moves faster
  sf::View cursorView;         /// < Field which is related to cursor position for item add dummies

  EditorMenu menu;             /// < The tool menu on top of the screen
  EditorItems eitems;          /// < The items menu on bottom of the screen
  EditorFB fb;                 /// < Indicator which shows that are we drawing to front or back of player
  EditorTemporaryShape s;      /// < Shape which is currently being drawn (if shape is being drawn anyway)

  int drawViews = 0xFF;        /// < List of enabled views. The hex numbers are the bitmasks (not used)
  float posx, posy;            /// < Position of camera
  Map* map;                    /// < The map file to be edited
  int mousex, mousey;          /// < Mouse position
  bool mclick_state=false;     /// < State of mouse click
  int toolSelection=-1;        /// < Selected tool
  int itemSelection=-1;        /// < Selected decorational item
  MapObject* newObj=NULL;      /// < Pointer to object which will be used cursor dummy
  int layer = 0;               /// < Layer where ground will be;
  
  bool saveNewMap=false;       /// < If true the user is asked the filename and name for the map
  std::string saveNewMapName;  /// < Name entered
  std::string saveNewMapFname; /// < Filename entered
  int saveNewMapState;         /// < State of saving new map
  bool saveNewKeyPressed;      /// < Avoid getting multiple times same input key

  // All of these are run inside run function
  void handleInput();          /// < Handle the keyboard input
  void handleToolPlacement();  /// < Handle placing and item or triangle point when tool is selected
  void handleItemPlacement();  /// < Handle placing a decorational item when one is selected
  void TemporaryShapeToMap();  /// < Convert currently drawn shape to MapObjectTriangles and place it to map
  void handleSaveNewMap(sf::RenderWindow& window, sf::Event& event); /// < Handle the saving of new map

  /**
   * Add theme related data to a map. This will insert theme related textures to a map
   * @map Map where to insert them
   * @theme index of theme to add
   */
  void addTheme(Map* map, int theme);

public:

  Editor();

  /**
   * Loads the map specified by the filename and if no file was specified then create new map with 
   * theme specified
   * @param filename Name of the map file to load
   * @param theme Theme for new map
   */
  void load(std::string filename, int theme);

  // Inherited functions from Screen and sf::Drawable
  void draw(sf::RenderTarget& target, sf::RenderStates states) const;
  int run(sf::RenderWindow& window, sf::Event& event);
  
};

#endif
