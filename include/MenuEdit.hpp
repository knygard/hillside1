#ifndef MENUEDIT_HPP
#define MENUEDIT_HPP
#include "Menu.hpp"
#include "Editor.hpp"

/**
 * Enumerator used on edit menu.
 * This is basically just a list of possible choices and the choice is changed 
 * when menu is activated and the menu choice text is changed accordingly
 */
class MenuEditEnumerator : public MenuChoiceCallBack
{
private:
  std::vector<std::string> values; /// < Choices on this enumerator
  std::string name;                /// < Name of this enumerator. Shown on the menu choice as prefix "Prefix: <selected value">
  int currentSelection = 0;        /// < Curently selected value. This will indicate the selected value

public:
  MenuEditEnumerator(std::string name) : name(name) {}

  void menuEvent(MenuChoice& who);

  /**
   * Returns the text representation of this enumerator. This is used when updating the menu choice text
   * This basically returns "<name>: <values[currentSelection]>"
   * @return The string containing the choice
   */
  std::string getText();

  /**
   * Returns the index of currently selected item
   */
  int getCurrentSelection();

  /**
   * Add choice to this enumerator
   */
  void addChoice(std::string choice);
};

// Forward declaration so that we can use MenuNewGame on MenuNewGameStartGame as owner
class MenuEdit;

/**
 * This is used to call Editor::load to load the selected map or theme when Edit choice is activated
 */
class MenuEditEdit : public MenuChoiceCallBack
{
private:
  MenuEdit* owner; /// < Owner menu. This is used to get the currently selected map and theme information
public:
  MenuEditEdit(MenuEdit* owner) : owner(owner) {}
  void menuEvent(MenuChoice& who);
};

/**
 * This class contains the Editor menu. There are three choices:
 * two enumerators which contains selections for map and theme and 
 * Edit choice which will load the data to editor and change gamestate to editor
 */
class MenuEdit : public Menu
{
private:
  MenuEditEnumerator theme; /// < Enumerator containing the theme value
  MenuEditEnumerator map;   /// < Enumerator containing the currently selected map value
  MenuEditEdit edit;        /// < Contains function which will be used to call Editor::load when Edit choice is activated
  std::vector<std::string> maplist; /// < List of maps from maplist.txt
  Editor& e;                /// < Reference to editor which is used

public:
  MenuEdit(Editor& e);

  /**
   * Return the currently selected map. Used by the edit when calling Editor::load
   */
  std::string getMap();

  /**
   * Return the currently selected theme. Used by the edit when calling Editor::load
   */
  int getTheme();

    /**
   * Return reference to the editor. Used by the edit when calling Editor::load
   */
  Editor& getEditor();
};

#endif
