#ifndef MAPITEM_HPP
#define MAPITEM_HPP

#include <sstream>
#include "MapObject.hpp"

/**
 * This class represents one decorational item on the map
 */
class MapObjectItem : public MapObject {

public:

  MapObjectItem(std::string texname, int type, std::istringstream& s);

  MapObjectItem(std::string texname, int type, float x, float y, float w, float h);

  virtual void updateTextures(std::map<std::string, sf::Texture*>& textures);

  void draw(sf::RenderTarget &target, sf::RenderStates states) const;

  std::string getAsText() const;
private:
  std::string texname;
  sf::Texture* t;
  sf::RectangleShape shape;
};

#endif
