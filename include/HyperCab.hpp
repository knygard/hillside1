#ifndef Hillside1_HyperCab_hpp
#define Hillside1_HyperCab_hpp

#include "TwoWheelVehicle.hpp"

/*
 * Four-wheel-drive Taxi vehicle
 */
class HyperCab : public TwoWheelVehicle
{

public:
  HyperCab(b2World* world);

};

#endif
