#ifndef Hillside1_Screen_hpp
#define Hillside1_Screen_hpp

//#include "Game.hpp"
#include <sstream> 
#include <SFML/Graphics.hpp>

#define GAMESTATE_MAINMENU 1
#define GAMESTATE_MAPSELECTION 2
#define GAMESTATE_HIGHSCORES 3
#define GAMESTATE_HIGHSCORES_MAPSELECTION 4
#define GAMESTATE_GAME 5
#define GAMESTATE_EDITOR 6
#define GAMESTATE_EDITMENU 7
#define GAMESTATE_FINISH 8


class Screen
{
protected:
  sf::Font font;

  Screen()
  {
    font.loadFromFile("bauerg.ttf");
  }

public :
  virtual int run(sf::RenderWindow& window, sf::Event& event) = 0;
};

#endif
