#ifndef FINISH_HPP
#define FINISH_HPP      
     
#include "Screen.hpp"


/**
 * Finish class
 *
 * A screen showing the results of a completed map
 *
 */  
  
class Finish : public Screen, sf::Drawable
{
private:
  //name of the player
  std::string name;
  std::string nametitle;
  
  sf::Font uifont;
  
  //objects appearing on screen
  sf::Text text;
  sf::Text points_text;
  std::stringstream points_ss;
  sf::Text ranking_text;
  std::stringstream ranking_ss;
  sf::Text completed_text;
  sf::RectangleShape background;
  
  //variables to be shown on the screen
  size_t points;
  size_t ranking;
  
  //timer for the pressing of the keys
  sf::Clock clock;

public:

  Finish();
  
  int run(sf::RenderWindow& window, sf::Event& event);
  
  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
  
  void setPoints(size_t p);
  void setRanking(size_t r);

  int processEvents(sf::Event& event);
  
  std::string getName();
};

#endif
