#ifndef MAPOBJECT_HPP
#define MAPOBJECT_HPP

#define MAPOBJ_IDENT_NONE     0
#define MAPOBJ_IDENT_VEHICLE  1
#define MAPOBJ_IDENT_COIN     2
#define MAPOBJ_IDENT_BOOST    3
#define MAPOBJ_IDENT_GOAL     4
#define MAPOBJ_IDENT_RANDOM   5

#define MAPTYPE_SKY      1
#define MAPTYPE_BG       2
#define MAPTYPE_FG       4
#define MAPTYPE_FRONT    8
#define MAPTYPE_PLAYER   16
#define MAPTYPE_EDITONLY 32

#include "Settings.hpp"
#include "Utilities.hpp"
#include <iostream>

// This is abstract class which represents one object on the map
class MapObject : public sf::Drawable
{
protected:
  int type;       // Render type. This defines the layer where this item will be drawn (sky, background, players, foreground, front,..)
  int identifier; // Unique for each type of object. This is used to identify different kinds of objects when colliding

public:
  
  MapObject(int type=MAPTYPE_FG, int identifier=MAPOBJ_IDENT_NONE) :
  type(type), identifier(identifier) { }

  //virtual ~MapObject();

  //virtual void draw(sf::RenderTarget&, sf::RenderStates) const = 0;
  virtual void update() {};

  int getType() {
    return type;
  };

  virtual void setPosition(float x, float y) {
    std::cout << "Warning: set position of object which does not support it!" << std::endl;
  }

  bool isCollectable() {
    if(identifier == MAPOBJ_IDENT_COIN ||
       identifier == MAPOBJ_IDENT_BOOST ||
       identifier == MAPOBJ_IDENT_GOAL)
      return true;
    else
      return false;
  }

  virtual void updateTextures(std::map<std::string, sf::Texture*>& textures) {
    /* This loads the actual textures which matches the names. This way the
     * objects does not have to refer to any map */
  }

  int getIdent() {
    return identifier;
  }
  
  virtual std::string getAsText() const {
    return "comment string representation of map object which does not have one\n";
  }

  virtual bool isDestroyed() {return false;}
};

std::ostream& operator<<(std::ostream& os, const MapObject& m);

#endif
