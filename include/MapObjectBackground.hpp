#ifndef MAPBACKGROUND_HPP
#define MAPBACKGROUND_HPP

#include "MapObject.hpp"

/*
 * MapObjectBackground class
 *
 * This class simply draws picture over the whole area.
 * If the centerpoint of view is outside of the picture area it offsets the picture. 
 */
class MapObjectBackground : public MapObject {
public:
  /**
   * Texture for this background
   */
  sf::Texture* background;
  /**
   * Shape for the image
   */
  sf::RectangleShape bg;
  /**
   * Offset of the background image
   */
  sf::Vector2<float> offsets;
  /**
   * Size of the background image
   */
  sf::Vector2<float> sizes;

  MapObjectBackground(sf::Texture* background, sf::Vector2<float> offsets, sf::Vector2<float> sizes,int type=MAPTYPE_SKY);
  void draw(sf::RenderTarget &target, sf::RenderStates states) const;
  void update();
  std::string getAsText() const;
};

#endif
