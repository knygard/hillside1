#ifndef SCOREMANAGER_HPP
#define SCOREMANAGER_HPP

#include "Menu.hpp"

/**
 * @brief Used for showing the high scores
 */
class ScoreManager : public MenuChoiceCallBack, public Menu
{
private:
  Game& mygame;
  ResultLoader rl;
public:
  ScoreManager(Game& g);
  
  void menuEvent(MenuChoice& who);

  /**
   * @brief Writes the high scores on the screen
   * @param game Reference to game
   */
  void update(std::string fname);
};


/**
 * @brief Used for selecting the map for the high scores
 */
class MapManager : public MenuChoiceCallBack, public Menu
{
private:
  Game& mygame;
  std::map<int, std::string> maps;
  std::map<int, std::string> mapnames;
  std::string fname;

public:
  MapManager(Game& g, std::string maplist);

  /**
   * @brief Returns filename of the selected map
   */
  void menuEvent(MenuChoice& who);

  std::string getMapFile() {return fname;}
};

#endif
