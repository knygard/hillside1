#ifndef RESULT_HPP
#define RESULT_HPP

/**
 * Result class
 *
 * Stores and calculates game results
 */

class Result
{
public:
  Result(std::string n, size_t t, size_t p) : name(n), time(t), points(p) {}
  
  int calculateGamePoints() {
    int game_points = time - 100*points;
    if(game_points > 0) {return game_points;} else {return 0;}
  }
  
  std::string getName() {return name;}
  size_t getTime() {return time;}
  size_t getPoints() {return points;}

private:
  std::string name;
  size_t time;
  size_t points;
};

#endif
