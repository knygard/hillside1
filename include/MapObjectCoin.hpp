#ifndef MAPOBJECTCOIN_HPP
#define MAPOBJECTCOIN_HPP

#include "MapObjectCollectable.hpp"

/**
 * MapObjectCoin class
 * Contains all information related to a coin
 */
class MapObjectCoin : public MapObjectCollectable 
{
public:
  // s is stream which contains string representation of the object contents
  MapObjectCoin(b2World* world, std::istringstream& s);
  MapObjectCoin(b2World* world, float x, float y);

  void init();

  // Draws the coin if it is not yet collected
  void draw(sf::RenderTarget &rt, sf::RenderStates states) const;

  std::string getName() const;

  b2Shape& getShape();

private:
  b2CircleShape myShape;
  sf::CircleShape coin;      // Graphical representation of coin
  sf::Texture cointexture;   // Texture of coin
  float m_radius;
};


#endif
