#ifndef MAPOBJECTVEHICLE_HPP
#define MAPOBJECTVEHICLE_HPP

#include "MapObject.hpp"
#include <vector>

/**
 * The abstract class of all vehicles
 * @see MapObject
 */
class MapObjectVehicle : public MapObject
{
private:
  const float boostAmount = 10000;

protected:
  /** Maximum vehicle speed */
  float maxSpeed;

  /** The amount of force to apply to tilting */
  float angularImpulse;

  /** The factor to apply to boost */
  float boostFactor;

  /** Points to the world where the vehicle is (owned by map) */
  b2World* world;

  /** Keeps track of the collected items by this vehicle */
  std::vector<int> collectables;

  /**
   * @brief Sets motor speed
   * @param speed (float)
   */
  virtual void setSpeed(float) = 0;

  /**
   * @brief Monitors the speed not to go over maxSpeed
   */
  virtual void limitSpeed() = 0;

  /**
   * @brief Applies angular impulse to bodies
   */
  void applyAngularImpulse(std::vector<b2Body*>);

  /**
   * @brief Deletes collected items by type
   * @param ident Identifier of items to be deleted
   */
  void deleteCollected(int ident);

public:
  MapObjectVehicle(b2World* world) :
  MapObject(MAPTYPE_PLAYER, MAPOBJ_IDENT_VEHICLE), world(world)
  { }

  virtual ~MapObjectVehicle()
  { }

  /**
   * @brief Sets player (vehicle) position (at start)
   */
  virtual void setPosition(float x, float y) = 0;

  /**
   * @brief Updates the graphics to match physics
   */
  virtual void update() = 0;

  /**
   * @brief Player commands the vehicle forward using this method
   * @param deltaTime to calculate average change of velocity
   */
  virtual void forward(float deltaTime) = 0;

  /**
   * @brief Player commands the vehicle forward using this method
   * @param deltaTime to calculate average change of velocity
   */
  virtual void backward(float deltaTime) = 0;

  /**
   * @brief Accelerates the vehicle
   * @param change (how much to increase motor speed)
   */
  virtual void accelerate(float32 change) = 0;

  /**
   * @brief Stops the motors immediately (100% breaking)
   */
  virtual void stop() = 0;

  /**
   * @brief Sets the vehicle motor to idle (comparable to N-gear)
   */
  virtual void idle() = 0;

  /**
   * @brief Applies boost to the vehicle velocity
   */
  virtual void boost() = 0;

  /**
   * @brief Tilts the vehicle into clockwise/anti-clockwise direction
   * @param clockwise (bool)
   */
  virtual void tilt(bool clockwise) = 0;

  /**
   * @brief Returns motor speed negated (so anti-lockwise is positive)
   */
  virtual float getMotorSpeed() const = 0;

  /**
   * @brief Returns vehicle (chassis) velocity
   * @result b2Vec2 velocity
   */
  virtual b2Vec2 getVelocity() const = 0;

  /**
   * @brief Returns vehicle position
   * @result b2Vec2 position
   */
  virtual b2Vec2 getPosition() const = 0;

  /**
   * @brief Adds item to collectables by identifier
   * @param ident Item identifier
   */
  void addCollectable(int ident);

  /**
   * @brief Returns list of collectable items
   *        filters by identifier if provided
   * @param ident Item identifier
   * @result std::vector<int> List of collectables
   */
  std::vector<int> getCollectables(int ident=0) const;
};

#endif
