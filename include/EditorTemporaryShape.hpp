#ifndef EDITORTEMPORARYSHAPE_HPP
#define EDITORTEMPORARYSHAPE_HPP

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "Settings.hpp"

// Draws cross which is used at the endpoints of lines to indicate points
#define EdDrawX(point) {						\
  glVertex3f((float)((point).x*20-5),(float)(-(point).y*20-5), 1.0);	\
  glVertex3f((float)((point).x*20+5),(float)(-(point).y*20+5), 1.0);	\
  glVertex3f((float)((point).x*20-5),(float)(-(point).y*20+5), 1.0);	\
  glVertex3f((float)((point).x*20+5),(float)(-(point).y*20-5), 1.0); }

// Different modes of operation
#define ED_TRIANGLE 1
#define ED_TRIANGLESTRIP 2
#define ED_TRIANGLEFAN 3
/**
  * EditorTemporaryShape
  *
  * This class is used to handle the shape which is currently being created.
  * This coitains list of all the already created triangles and keeps track of
  * partly created triangle state and handles drawing of different kinds of
  * triangles.
  * This does also handle drawing of the shape and conenction to mouse cursor.
  */
class EditorTemporaryShape : public sf::Drawable {
private:
  std::vector<sf::Vector3<sf::Vector2f>> points; /// < Points of already made triangles
  sf::Vector2f p1,p2;               /// < temporary vectors for triangle
  size_t phase;                     /// < phase of triangle draw
  int type=1;                       /// < type of shape to be drawn
  sf::ConvexShape shape;            /// < Dummy to avoid problems with sfml and opengl positioning
  float curx, cury;                 /// < cursor position
  float sx=0;                       /// < Snapped point x
  float sy=0;                       /// < Snapped point y
  bool snap_on = false;             /// < Is there some point nearby where the point will be snapped to

public:
  EditorTemporaryShape() : phase(0), curx(0), cury(0) {}
  
  /**
   * Updates the position of the cursor to the temporary shape.
   * This is used to draw the line to the cursor when one point is drawn or
   * draw triangle with one endpoint at cursor when two points are already drawn.
   * This also updates the snap position and status if there are one existing point nearby cursor
   */
  void update(float x, float y);
	
  /**
   * Returns the list of full ready triangles
   * @return Reference to list of triangles
   */
  std::vector<sf::Vector3<sf::Vector2f>>& getTriangles();
  
  /**
   * Clear the contents and set the type of triangles to be created
   * @param type Sets the type of new triangles (as defined above ED_TIANGLE*)
   */
  void clear(int type);

  /**
   * Clears made triangles and sets phse to 0
   */
  void clear();

  /**
   * Adds new point to shape. This function does take care of handling different kinds of shapes
   * This way the rendering etc. can be kept simple.
   * Basicly the triangle which is currently under construction is handled differently than ready ones.
   * The phase represents how many points are already drawn so:
   * if 0 is drawn then set the p1 to match the point and set phase to 1
   * it 1 is drawn then set the p2 to match the point and set phase to 2
   * If normal triangles are drawn the phase 2 creates new triangle from p1,p2 and cursor pos and phase->0
   * If trianglefan is drawn the first point will remain always the first one and the second is last drawn point(cur)
   * If trianglestrip is drawn the p1 and p2 will be p2 and cursor and phase will remain 2.
   * @param x position in x-axis
   * @param y position in y-axis
   */
  void addPoint(float x, float y);

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
};

#endif
