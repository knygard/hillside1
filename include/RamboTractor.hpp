#ifndef Hillside1_RamboTractor_hpp
#define Hillside1_RamboTractor_hpp

#include "TwoWheelVehicle.hpp"

/**
 * Rear-wheel-drive tractor
 */
class RamboTractor : public TwoWheelVehicle
{

public:
  RamboTractor(b2World* world);

  // Overriden because front wheel has to be over chassis
  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
};

#endif
