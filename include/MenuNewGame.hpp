#ifndef MENUNEWGAME_HPP
#define MENUNEWGAME_HPP
#include "Menu.hpp"
#include "Game.hpp"

/**
 * Enumerator used by some menu choice which can be used to select one value from many choices.
 * When menu choice is activated this is called and this will choose next choice from its values
 * and update the menu choice text accordingly.
 */
class MenuNewGameEnumerator : public MenuChoiceCallBack {
private:
  std::vector<std::string> values; /// < Values which will be enumerated through
  std::string name;                /// < Name of this enumerator shown on menu choice text
  int currentSelection = 0;        /// < Currently selected value

public:
  MenuNewGameEnumerator(std::string name) : name(name) {}
  void menuEvent(MenuChoice& who);

  /**
   * Returns the text representation of this enumerator. This is used when updating the menu choice text
   * This basically returns "<name>: <values[currentSelection]>"
   * @return The string containing the choice
   */
  std::string getText();

  /**
   * Returns the index of currently selected item
   */
  int getCurrentSelection();

  /**
   * Add choice to this enumerator
   */
  void addChoice(std::string choice);
};

// Forward declaration so that we can use MenuNewGame on MenuNewGameStartGame as owner
class MenuNewGame;

/**
 * This will be called when start game choice is activated. This just sets up the
 * game state based on the choices made by the user
 */
class MenuNewGameStartGame : public MenuChoiceCallBack {
private:
  Game& g;            /// < Reference to game which will be called
  MenuNewGame* owner; /// < Owner menu. This is used to get the user choices from the menu
public:
  MenuNewGameStartGame(MenuNewGame* owner, Game& g) : owner(owner), g(g) {}
  void menuEvent(MenuChoice& who);
};

class MenuNewGame : public Menu {
private:
  MenuNewGameEnumerator pl1car;      /// < Enumerator for Player 1 car
  MenuNewGameEnumerator map;         /// < Enumerator for the map
  MenuNewGameStartGame startgame;    /// < Menu choice to start the game
  std::vector<std::string> maplist;  /// < List of maps shown on the menu
  Game& g;                           /// < Reference to game used
  std::map<std::string, std::string> readLevels(std::ifstream& in); /// < Function which will load the levels from file

public:
  MenuNewGame(Game& g);
  /**
   * Returns the car choice
   */
  int getPl1car();

  /**
   * Returns the map choice
   */
  std::string getMap();
};

#endif
