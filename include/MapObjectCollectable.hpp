#ifndef MAPOBJECTCOLLECTABLE_HPP
#define MAPOBJECTCOLLECTABLE_HPP
#include <Box2D/Box2D.h>
#include <sstream>
#include "MapObject.hpp"
#include "MapObjectVehicle.hpp"

/*
 * MapObjectCollectable class
 *
 * Inherited by the coins, boosts and goals
 *
 **/
class MapObjectCollectable : public MapObject
{
public:
  // s is stream which contains string representation of the object contents
  MapObjectCollectable(b2World* world, std::istringstream& s, int type);
  MapObjectCollectable(b2World* world, float nx, float ny, int type);
  void destroy();
  void collect();
    virtual b2Shape& getShape() = 0;
  bool isDestroyed();
  void setPosition(float newx,float newy);
  virtual std::string getName() const = 0;
  std::string getAsText() const;
  bool addToWorld();

protected:
  bool collected;            // True if collected (hide)
  float x,y;
  b2World* world;
  b2Body* body;
};

class CollectableListener : public b2ContactListener
{
  void BeginContact(b2Contact* contact);
  void EndContact(b2Contact* contact);
};

#endif
