#ifndef Hillside1_TwoWheelVehicle_hpp
#define Hillside1_TwoWheelVehicle_hpp

#include <iostream>
#include <iomanip> // std::setprecision
#include "MapObjectVehicle.hpp"

/**
 * @brief The abstract class of all vehicles with two wheels,
 *        inherits MapObjectVehicle
 * @see MapObjectVehicle
 */
class TwoWheelVehicle : public MapObjectVehicle
{

protected:
  b2FixtureDef rearWheelFixture;
  b2FixtureDef frontWheelFixture;
  b2FixtureDef chassisFixture;
  b2WheelJointDef rearWheelJoint;
  b2WheelJointDef frontWheelJoint;

  b2Body* b2Chassis;
  b2Body* b2RearWheel;
  b2Body* b2FrontWheel;
  b2WheelJoint* b2RearSpring;
  b2WheelJoint* b2FrontSpring;

  sf::ConvexShape sfChassis;
  sf::CircleShape sfRearWheel;
  sf::CircleShape sfFrontWheel;

  std::string chassisTexturePath;
  sf::Texture chassisTexture;
  std::string wheelTexturePath;
  sf::Texture wheelTexture;

  float chassis_distance; // Chassis distance from ground
  b2Vec2 chassisVertices[6];

  /** Enum of front/back/four-wheeldrive */
  int driveType;

  float acceleration;
  float rearWheelRadius;
  float frontWheelRadius;
  b2Vec2 rearWheel_position;
  b2Vec2 frontWheel_position;

  void initialize();

  /** Maximum motor speed */
  float maxMotorSpeed;

  virtual void setSpeed(float);

public:

  enum DriveType
  {
    RearWheelDrive,
    FrontWheelDrive,
    FourWheelDrive
  };

  TwoWheelVehicle(b2World*);

  void setPosition(float x, float y);

  virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
  void update();

  void forward(float deltaTime);
  void backward(float deltaTime);
  void stop();
  void accelerate(float32 change);
  void idle();
  void tilt(bool clockwise);
  void boost();

  void limitSpeed();

  b2Vec2 getPosition() const {
    return b2Chassis->GetPosition();
  }

  b2Vec2 getVelocity() const {
    return b2Chassis->GetLinearVelocity();
  }

  float32 getMotorSpeed() const {
    return -b2RearSpring->GetMotorSpeed();
  }

  /* Custom Methods */

  /**
   * @brief Disables motors
   */
  void disableMotor();

  /**
   * @brief Enables motors based on driveType (enum)
   */
  void enableMotor();
  
};

#endif
