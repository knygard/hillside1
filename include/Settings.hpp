#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768
#define PIX_PER_M 20

struct windowSettings {
  int width;
  int height;
};

#endif
