#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Map.hpp"
#include "Settings.hpp"
#include "ResultLoader.hpp"

#include <SFML/Graphics.hpp>
//#include <Box2D.h>
#include <sstream>
#include <cmath> // abs


/**
 * Heads up display class
 *
 * This class draws the player status to the screen
 * If no player is defined then static text is printed as status
 * This is called by PlayerData class and probably shouldn't be used without it
 */
class PlayerHud : public sf::Drawable
{
private:

  /// Padding from borders
  float padding;

  sf::Clock clock;
  sf::Font uifont;

  sf::Text points_text;
  std::stringstream points_ss;

  sf::Text speed_text;
  std::stringstream speed_ss;

  sf::Text boosts_text;
  std::stringstream boosts_ss;

  sf::Text time_text;
  std::stringstream time_ss;

public:

  PlayerHud();

  /**
   * @brief Update the points, speed, boosts and time value
   */
  void update(int points, int boosts, int speed);

  /**
   * @brief Draw the contents
   */
  void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};


/**
 * Player class
 *
 * All the data to related one player including the graphical view, player state, etc.
 * This represents the player view on the screen (or on some other buffer)
 * If player is not specified then this just gives view to the game world which can be moved by calling setPosition
 */
class Player : public sf::Drawable
{

private:

  bool keyWasPressed;         ///< Keeps track whether key was pressed, released on keyrelease, true = pressed

  sf::Vector3<float> color;   ///< Player color

  // These are the different layers of the game view
  sf::View gameView;          ///< Moving game field
  sf::View staticView;        ///< The part which will be static like points and speed
  sf::View skyView;           ///< Sky behind the game area which moves slower than the game area
  sf::View frontView;         ///< The area in front of the game area which moves faster


  float defaultx, defaulty;   ///< Position of camera if no player was defined
  PlayerHud* hud;             ///< This contains all the texts etc. related to player view
  MapObjectVehicle* vehicle;  ///< Vehicle assigned to player
  Map* map;                   ///< The map which will be linked to this player
  int id;                     ///< Player id. can show the player number if not zero

  sf::Clock clock;


public:

  Player(int id, MapObjectVehicle* vehicle, Map* map);

  ~Player();

  /**
   * @brief Updates the player view position.
   */
  void update();

  /**
   * @brief Sets the position that this view shows. This works only if player==NULL.
   */
  void setPosition(float x, float y);

  /**
   * @result Player (vehicle) position
   */
  const b2Vec2 getPosition() const;

  /**
   * @brief Zooms the view. Note: This is relative to previous state
   */
  void zoom(float amount);

  /**
   * @brief Draws the game area and status
   * @param target RenderTarget
   * @param states RenderStates
   */
  void draw(sf::RenderTarget& target, sf::RenderStates states) const;

  int getPoints() {
    return getCoins();  // !!! Fix: points != coins
  }

  /**
   * @result List (vector) of coins collected by player's vehicle
   */
  int getCoins() {
    return vehicle->getCollectables(MAPOBJ_IDENT_COIN).size();
  }

  /**
   * @result List (vector) of boosts collected by player's vehicle
   */
  int getBoosts() {
    return vehicle->getCollectables(MAPOBJ_IDENT_BOOST).size();
  }

  /**
   * @result List (vector) of goals (usually just one)
   *         collected by player's vehicle
   */

  int getGoal() {
    return vehicle->getCollectables(MAPOBJ_IDENT_GOAL).size();
  }

  /**
   * @result Returns elapsed time
   */

  int getTime() {
    return clock.getElapsedTime().asMilliseconds();
  }

  /**
   * @result Pointer to player's vehicle
   */
  MapObjectVehicle* getVehicle() {
    return vehicle;
  }

  /**
   * @result Pointer to map associated with player
   */
  Map* getMap() {
    return map;
  }

  /**
   * @brief Process events related to player
   * @param event Window event
   * @param deltaTime deltaTime
   */
  void processEvent(sf::Event event, float deltaTime);

  /**
   * @brief Player keyboard mapping
   * @param deltaTime deltaTime
   */
  void keyboard(float deltaTime);

  /**
   * @brief Saves player result to results file
   * @param fname Map filename
   * @param name Player name
   * @param points Player points
   * @param time Player elapsed time
   */
  void saveResult(std::string fname, std::string name, size_t points, size_t time);

  /**
   * @brief Calculates player ranking on map
   * @param fname File name for map
   */
  size_t getRanking(std::string fname);
};


#endif
