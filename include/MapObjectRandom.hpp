#ifndef MAPOBJECTRANDOM_HPP
#define MAPOBJECTRANDOM_HPP

#include "MapObjectCollectable.hpp"

/**
 * MapObjectRandom class
 *
 * Item which can be some unexpected out of nowhere appearing power up.
 * Normally this should be invisible until randomly revealed and changed to something
 *
 */
class MapObjectRandom : public MapObjectCollectable 
{
public:

  MapObjectRandom(b2World* world, std::istringstream& s);

  MapObjectRandom(b2World* world, float x, float y);

  void init();

  std::string getAsText() const;

  void draw(sf::RenderTarget &rt, sf::RenderStates states) const;

  std::string getName() const;

  b2Shape& getShape();

private:
  b2CircleShape myShape;
  sf::CircleShape random;      // Graphical representation of random
  sf::Texture randomtexture;   // Texture of random
  float m_radius;
};


#endif
