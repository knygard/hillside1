#ifndef MAPOBJECTGOAL_HPP
#define MAPOBJECTGOAL_HPP

#include "MapObjectCollectable.hpp"

/**
 * MapObjectGoal class
 * Contains all information related to a goal
 *
 */
class MapObjectGoal : public MapObjectCollectable
{
public:

  MapObjectGoal(b2World* world, std::istringstream& s);
  MapObjectGoal(b2World* world, float x, float y);

  void init();

  std::string getName() const;
  
  void draw(sf::RenderTarget &rt, sf::RenderStates states) const;

  b2Shape& getShape();

private:
  b2PolygonShape polyShape;
  sf::ConvexShape goal;      // Graphical representation of goal
  sf::Texture goaltexture;   // Texture of goal
};


#endif
