#ifndef MAPOBJECTTRIANGLES_HPP
#define MAPOBJECTTRIANGLES_HPP

#include "MapObject.hpp"
#include <SFML/OpenGL.hpp>

/**
 * This class represents the ground (or background ground) object made out of one or more triangles.
 */
class MapObjectTriangles : public MapObject {
public:
  MapObjectTriangles(b2World* world, std::string texname, std::vector<sf::Vector3<sf::Vector2f>>& triangles, bool impassable, int type);
  MapObjectTriangles(b2World* world, std::string texname, bool impassable, std::istringstream& in_line, int type);

  void updateTextures(std::map<std::string, sf::Texture*>& textures);
  std::string getAsText() const;  
  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;

private:
  bool impassable;               ///< Should this
  sf::ConvexShape shape;         ///< Dummy shape used to get around sfml bug related to switching views and opengl
  std::string texname;           ///< Name of texture
  sf::Texture* t;                ///< Texture of the ground
  b2Body* shapeBody;             ///< Body for the physics engine
  std::vector<sf::Vector3<sf::Vector2f>> triangles; /// < List of ground triangles
};


#endif
