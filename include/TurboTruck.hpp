#ifndef Hillside1_TurboTruck_hpp
#define Hillside1_TurboTruck_hpp

#include "TwoWheelVehicle.hpp"

/**
 * Rear-wheel-drive monster truck
 */
class TurboTruck : public TwoWheelVehicle
{

public:
  TurboTruck(b2World* world);

};

#endif
