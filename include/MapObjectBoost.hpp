#ifndef MAPOBJECTBOOST_HPP
#define MAPOBJECTBOOST_HPP
#include "MapObjectCollectable.hpp"
#include <sstream>

/*
 * MapObjectBoost class
 *
 * Contains all information related to a boost
 *
 **/
class MapObjectBoost : public MapObjectCollectable
{
public:
  MapObjectBoost(b2World* world, std::istringstream& s);
  MapObjectBoost(b2World* world, float x, float y);

  void init();

  std::string getName() const;
  
  void draw(sf::RenderTarget &rt, sf::RenderStates states) const;

  b2Shape& getShape();

private:
  b2CircleShape myShape;
  sf::CircleShape boost;      // Graphical representation of boost
  sf::Texture boosttexture;   // Texture of boost
  float m_radius;
};


#endif
