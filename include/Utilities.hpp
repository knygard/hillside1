#ifndef Hillside1_Utilities_hpp
#define Hillside1_Utilities_hpp

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include "Settings.hpp"

// Use with older Box2D
// #define ApplyAngularImpulse(a) ApplyAngularImpulse(a, true)

namespace B2toSFRenderer {

  /**
   * @brief Creates SFML convex shape from Box2D polygon
   */
  sf::ConvexShape PolygonToSFConvex(b2PolygonShape&);

  /**
   * @brief Create SFML circle shape from Box2D circle
   */
  sf::CircleShape CircleToSFCircle(b2CircleShape&);
}

#endif
