#ifndef Hillside1_Game_hpp
#define Hillside1_Game_hpp

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <sstream>
#include <utility> 
#include <cmath> 
#include "Settings.hpp"
#include "Screen.hpp"
#include "Map.hpp"
#include "Player.hpp"

namespace {
  const float32 timeStep = 1.0f / 60.0f;
  const int32 velocityIterations = 8;
  const int32 positionIterations = 3;
}

/**
 * Game class 
 *
 * The main part of the program
 */
class Game : public Screen, public sf::Drawable
{
private:
  size_t mapnum;
  std::vector<Player*> players;
  sf::Clock clock;
  sf::Time timeSinceLastUpdate;
  Map* map;

public:

  Game();

  ~Game();
  
  /**
  * @brief resets the game to the given parameters
  * @param player_number The number of players in the game
  * @param pl1vehicle the vehicle of player 1
  * @param pl2vehicle the vehicle of player 2
  * @param the filename of the map
  */
  
  void resetState(int pl1vehicle, std::string m);
  
  /**
  * @brief clears the player vector
  */
  
  void resetState();
  
  /**
  * @brief runs the game
  * @return GAMESTATE of the game
  * @param window target to which objects are drawn
  * @param 
  */

  int run(sf::RenderWindow& window, sf::Event& event);
  
  /**
  * @brief sets the map
  * @param filename of the map
  */

  void setMap(size_t m);
  
  /**
  * @brief getter for the players
  * @return players of the game 
  */

  std::vector<Player*> getPlayers();
  
  /**
  * @brief runs the game
  * @return a pointer to the specified vehicle
  * @param id of the vehicle
  */
  
  MapObjectVehicle* getVehicle(int id);
  
  /**
  * @brief draws the players
  * @param rt target to which objects are drawn
  * @param state renderstate for the drawing
  */

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
};

#endif
