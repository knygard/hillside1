#ifndef MAPOBJECTSP_HPP
#define MAPOBJECTSP_HPP
#include <sstream>
#include "MapObject.hpp"

/**
 * MapObjectStartpoint class
 * This should be the point where the player will be initially
 */
class MapObjectStartpoint : public MapObject
{
public:
  MapObjectStartpoint(std::istringstream& s);
  MapObjectStartpoint(float x, float y);
  void init();
  std::string getAsText() const;
  void draw(sf::RenderTarget &rt, sf::RenderStates states) const;

private:
  sf::CircleShape shape;      // Graphical representation of random
  sf::Texture texture;   // Texture of random
  float x,y;
};

#endif
