#ifndef MENU_HPP
#define MENU_HPP

#include <iostream>
#include <vector>
#include <string>
#include "Screen.hpp"
#include "Game.hpp"

// Forward declaration for MenuChoice so that it can be used in callback
class MenuChoice;

/**
 * This is pure abstract class which can be added to some menu item. The
 * Menu item calls menuEvent function when it has been activated.
 */
class MenuChoiceCallBack {
public:
  /**
   * This will be called by MenuChoice when it has been activated
   * @param who What MenuChoice did call this.
   */
  virtual void menuEvent(MenuChoice& who) = 0;
};

/**
 * This class represents one single menu entry on Menu.
 * This does contain all the logic and rendering related to one menu choice.
 * When menu item is activated by the used it will call function MenuChoiceCallBack if defined
 * After that it will change the state of the game to nextState if changemenu is true
 */
class MenuChoice : public sf::Drawable {
private:
  sf::Text name;              /// < The text of this choice shown on menu
  int id;                     /// < Id which can be used to identify this option for example from callback
  bool changemenu;            /// < If true activating this menu choice will change the game status
  int nextState;              /// < The new game status which will be returned by the menu when this item is activated
  MenuChoiceCallBack* callme; /// < If not NULL this function will be called when this menu choice is activated
  int ypos;                   /// < Position of this choice on the menu. This is as screen coordinates on y-axis. 

public:
  MenuChoice(std::string text, int state, int x, int y, sf::Font& font, bool changemenu=true, MenuChoiceCallBack* call=NULL, int id=0);

  /**
   * Sets this item as selected. This just does change the color of this choice, (false=unselected,white, true=selected,red)
   * @sel New selected status
   */
  virtual void setSelected(bool sel);

  /**
   * Set the text of this choice to something else
   * @param newtitle New title for the text
   */
  virtual void setText(std::string newtitle);

  /**
   * This is called by the Menu class when this option has been clicked by user
   * This does only call callback callme if it is not NULL
   */
  virtual void activate();

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;

  virtual int  getId() { return id; }
  virtual void setId(int newid) { id=newid; }
  virtual int  getNextState() { return nextState; }
  virtual void setNextState(int newstate) { nextState = newstate; }
  virtual bool getChangeMenu() { return changemenu; }
  virtual void setChangeMenu(bool value) { changemenu = value; }

};

class Menu :  public sf::Drawable, public Screen
{

private:
  size_t position;                  /// < Currently selected MenuChoice
  std::vector<MenuChoice*> choices; /// < choices which are shown on this menu
  sf::Texture logoTexture;          /// < Texture for the logo
  sf::Sprite logo;                  /// < Logo shown on menu
  sf::RectangleShape background;    /// < Background image of the menu
  sf::Font font;                    /// < Font of the menu
  int default_state;                /// < This is the state of this menu. It will be returned as game state when this menu is run and nothing is activated
  int prev_key_event=0;             /// < Previous key event. this is used to prevent multiple key events from same key
  Game* g;                          /// < game status for which this menu is bound to. This is only used to draw the game status on the background of the menu

public:
  Menu(std::string name, int default_state, Game* g=NULL);

  /**
   * Adds new choice to this menu
   * @param name Name of this choice which will be shown on the menu
   * @param state Next gamestate which will be returned if this option is activated
   * @param changemenu Change the state only if this is true
   * @param call MenuChoiceCallBack::menuEvent will be called if this choice is activated
   * @param Id of this menu which can be used somewhere else to identify this item
   */
  void addItem(std::string name, int state, bool changemenu=true, MenuChoiceCallBack* call=NULL, int id=0);  
  
  /**
   * Clears the menu. This just removes all choices from menu.
   */
  void clear();

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
  int run(sf::RenderWindow& rw, sf::Event& event);

  /**
   * Handle the keyboard input from sfml io event handler
   */
  virtual int processEvents(sf::Event& event);
  
};

#endif
