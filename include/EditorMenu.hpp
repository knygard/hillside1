//
//  EditorMenu.hpp
//
//  This file contains the level editor menus.
//
#ifndef EDITORMENU_HPP
#define EDITORMENU_HPP
#include <SFML/Graphics.hpp>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include "Settings.hpp"

/**
 * EditorButton
 *
 * Graphical representation of one editor ui button 
 */
class EditorButton : public sf::Drawable {
private:
  sf::Texture image;          /// < Image for the button
  sf::RectangleShape shape;   /// < Shape for image
  sf::RectangleShape shape2;  /// < Shape for image
  std::string name;           /// < Name of this tool which will be shown on description
  int id;                     /// < Identifier of this tool
  int x,y;                    /// < Location of this button
  bool subpic;                /// < Does this button have sub picture (like item buttons does have)

public:
  EditorButton(std::string filename, std::string name, int id, int x, int y, bool subpic=false);
  /**
   * Sets the subpicture texture
   * @param tex The texture to use
   */
  void setTexture2(sf::Texture* tex);

  /**
   * Returns the name of this tool to be shown on description
   * @return The name of this tool
   */
  std::string& getName();

  /**
   * Returns the id of the buttons. This is used to identify different tools
   * @return Numeric id of this tool
   */
  int getId();

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
};

/**
 * EditorFB
 *
 * Graphical representation of front/back button
 */ 
class EditorFB : public sf::Drawable {
private:
  sf::Texture front;          /// < Image used when front is selected
  sf::Texture back;           /// < Image used when back is selected
  sf::RectangleShape shape;   /// < Shape for image
  int x,y;                    /// < Location of this button
  int state=0;                /// < Current state of the indicator

public:
  /**
   * Creates new front/back indicator
   * @param x position on x-axis
   * @param y position on y-axis
   */
  EditorFB(int x, int y);

  /**
   * Sets the state of the button (0=back, 1=front)
   * @newstate New state of the indicator
   */
  void setState(int newstate);

  void draw(sf::RenderTarget& rt, sf::RenderStates states) const;
};

// Button spacing on menu
#define ED_BS 73
// constants for the button id numbers
#define EDBTN_BOOST 0
#define EDBTN_COIN 1
#define EDBTN_DELETE 2
#define EDBTN_EDIT 3
#define EDBTN_GOAL 4
#define EDBTN_ITEM 5
#define EDBTN_LOAD 6
#define EDBTN_PL1 7
#define EDBTN_PL2 8
#define EDBTN_RANDOM 9
#define EDBTN_SAVE 10
#define EDBTN_TRIANGLES 11
#define EDBTN_TRIANGLEFAN 12
#define EDBTN_TRIANGLESTRIP 13
// Offset of the top menu
#define BTN_OFFSET ED_BS*3

/**
 * EditorMenu
 *
 * Editor tool menu. This contains the list of tools which can be seen on top on the editor screen.
 */
class EditorMenu : public sf::Drawable
{
private:

  sf::Font uifont;                        /// < Font used in this menu
  std::vector<EditorButton*> buttons;     /// < List of tool buttons on this menu
  int selection=0;                        /// < Currently selected item
  int hilight=-1;                         /// < Currently hilighted item

  sf::Texture selected;                   /// < Texture for selection square
  sf::Texture hilighted;                  /// < Texture for hilight square
  sf::RectangleShape sel;                 /// < Rectanlge shape for selection
  sf::RectangleShape hil;                 /// < Rectangle shape for hilight

  sf::Text desc;                          /// < Desctiprtion text for tool
  sf::Text coords;                        /// < Coordinate text
  bool activated=false;                   /// < Menu was activated (to avoid multiple activations)

public:
  EditorMenu();

  /**
   * Update the menu status with mouse status. This will select tool if activated is true and cursor is on top of
   * a button
   * @param x Mouse x position
   * @param y Mouse y position
   * @param activate Is button pressed
   */
  void update(int x, int y, bool activate);

  /**
   * Sets the currently selected tool
   * @param s new tool selection
   */
  void setSelection(int s);
  
  /**
   * Returns the button number of currently selected tool
   * Note: This is not the id so this shouldn't be used to identify the selection
   * @return The index of selected button
   */
  int getSelection();

  /**
   * Sets the description text to something else.
   * @newdesc New description
   */
  void setDesc(std::string newdesc);

  /**
   * Returns the id of currently selected tool
   * @return Id of currently selected tool.
   */
  int getId();

  /**
   * Set which button is hilighted.
   * @param h Index of button to be hilighted
   */
  void setHilight(int h);

  /**
   * Get the hilighted button
   * @return Index of hilighted button
   */
  int getHilight();

  void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

/**
 * This class represents one item on the itemlist on bottom of the screen
 */
class EditorItem {
public:
  sf::Texture* tex;      /// < Texture of the item
  std::string name;      /// < Name of the item (texture identifier on map)
  std::string filename;  /// < Filename of the item texture
  sf::Vector2f size;     /// < Dimensions of the texture
  EditorItem(sf::Texture* tex, std::string name, std::string filename, sf::Vector2f size) :
    tex(tex), name(name), filename(filename), size(size) {}
};

/**
 * Items manager
 *
 * This class handles loading and showing the items. This is also the item list on bottom of the screen
 */
class EditorItems : public sf::Drawable
{
private:

  std::vector<EditorItem*> items;         /// < List of items

  sf::Font uifont;                        /// < Font used in this menu
  std::vector<EditorButton*> buttons;     /// < Buttons shown. Arrows and items.
  int selection=1;                        /// < Currently selected item
  int hilight=-1;                         /// < Currently hilighted item

  sf::Texture selected;                   /// < Texture for selection square
  sf::Texture hilighted;                  /// < Texture for hilight square
  sf::RectangleShape sel;                 /// < Rectanlge shape for selection
  sf::RectangleShape hil;                 /// < Rectangle shape for hilight

  bool wasActivated=false;                /// < Was this activated last time. This is used to avoid multiple activations
  int offset=0;                           /// < Scroll offset of the itemlist

public:
  EditorItems();

  /**
   * Get the list of all items
   * @return The itemlist
   */
  std::vector<EditorItem*>& getItems();

  /**
   * Load items from decorations.txt
   */
  void loadItems();

  /**
   * Update the mouse position and button state for menu
   * @param x Mouse x position
   * @param y Mouse y position
   * @param activate Is mouse button pressed
   */
  void update(int x, int y, bool activate);

  /**
   * Sets the selected button
   * @param s Button index to select
   */
  void setSelection(int s);

  /**
   * Returns the index of button which is selected
   * @return The index of selected button
   */
  int getSelection();

  /**
   * Returns the id of selected button.
   * @return The id of selected button
   */
  int getId();

  /**
   * Sets the hilighted button
   * @param h Index of button to be hilighted
   */
  void setHilight(int h);

  /**
   * Return the index of hilighted button
   * @return Index of hilighted button
   */
  int getHilight();

  /**
   * Returns the data of currently selected item
   * @return The itemdata
   */
  EditorItem* getItem(int item);

  void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
