#ifndef Hillside1_ResultLoader_hpp
#define Hillside1_ResultLoader_hpp

/**
 * ResultLoader class
 *
 * Loads game results from file
 */

class ResultLoader
{

public:  

  ResultLoader() {}
  
  std::vector<Result> getResults(std::string filename)
  {
    std::vector<Result> results;
    std::stringstream ss;
    ss << "Results_" << filename;
    std::ifstream infile(ss.str());
    std::string name;
    size_t time;
    size_t points;
    while(infile >> name >> time >> points)
    {
      Result res(name, time, points);
      results.push_back(res);
    }
    return results;
  }
  
  std::vector<std::pair<std::string, size_t>> getHighScores(std::string filename, size_t n)
  {
    std::vector<Result> results = getResults(filename);
    std::vector<std::pair<std::string, size_t>> highscores;
    size_t i = 0;
    for(auto it = results.begin(); it != results.end(); it++)
    {
      if(i < n) {
        std::pair<std::string, size_t> p = std::make_pair(it->getName(), it->calculateGamePoints());
        highscores.push_back(p);
        i++;
      } else {break;}
    } 
    return highscores;   
  }
};

#endif
