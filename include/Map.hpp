#ifndef MAP_HPP
#define MAP_HPP
#define BGSPEED 0.2
// These are used in MapObject type which indicates on which layer(s) the object will de drawn.

#include <iostream>
#include <iomanip>
#include <iterator>
#include <string>
#include <fstream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Settings.hpp"
#include "MapObject.hpp"
#include "MapObjectItem.hpp"
#include "MapObjectVehicle.hpp"
#include "MapObjectCollectable.hpp"
#include "MapObjectCoin.hpp"
#include "MapObjectBoost.hpp"
#include "MapObjectGoal.hpp"
#include "MapObjectBackground.hpp"
#include "MapObjectTriangles.hpp"
#include "MapObjectRandom.hpp"
#include "MapObjectStartpoint.hpp"
#include "TurboTruck.hpp"
#include "HyperCab.hpp"
#include "RamboTractor.hpp"
#include "Result.hpp"

class Map {
protected:

  sf::Vector2f playerStartPoint;

  std::vector<MapObject*> objects;                   /// < Objects in this map
  std::map<std::string, sf::Texture*> textures;      /// < Textures on this map
  std::map<std::string, std::string> textures_fname; /// < Filenames of the textures
  
  CollectableListener cl; /// < Listener for collecting coins.
  b2World* world;         /// < Box2d world object. This contains the world for physics engine
  std::string filename;   /// < Filename from which the map is read

public:

  Map();

  const std::vector<MapObject*>* getObjects() const;
  
  std::map<std::string, sf::Texture*>& getTextures();

  const std::map<std::string, std::string>* getTextureFilenames() const;

  ~Map();

  /**
   * Returns texture matching the texture id. If no match is found create a new texture and return it.
   * If filename is specified also load texture image from file to that texture container.
   * @param id The string id of the texture
   * @param fname Filename which will be loaded to texture
   * @return The texture matching the id
   */
  sf::Texture* loadTexture(std::string id, std::string fname = "");

  /**
   * Constructor which loads map from file specified
   * @param mapfilename filename of map to be loaded
   */
  Map(std::string mapfilename);

  /**
   * Returns the b2World used by physics engine
   * @return b2World object
   */
  b2World* getWorld();
  
  /**
   * Return the filanme where this map was loaded from
   * @return The filename as string
   */
  std::string getMapfilename();

  void saveMap(std::string filename);

  sf::Vector2f getPlayerStartPoint() const {
    return playerStartPoint;
  }

  /**
   * Add new object to this map
   * @param m MapObject to be adde
   */
  void addObject(MapObject* m);
  
  /**
   * Destroy collectables on map
   */
  void destroyCollectables();

  /**
   * Update map item positions  
   */
  void update() ;

  /**
   * This is almost like sf::Drawable except that this takes also typemask which tells which parts of
   * the screen should be drawn
   */
  void draw(sf::RenderTarget &target, sf::RenderStates states, int typemask) const;

};

std::ostream& operator<<(std::ostream& os, const Map& m);
#endif
