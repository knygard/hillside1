![](https://bytebucket.org/knygard/hillside1/raw/fb974402c5880e9edc2a3d0c522ac79135243e52/logo.png)

# Hillside racing

This is a game developed on Aalto University's C++ course on fall 2013. The game is side-scrolling Elastomania-style racing game, where you are supposed to complete levels by collecting points. You have three different vehicles to choose from and many different levels

* [See project documentation PDF](http://bytebucket.org/knygard/hillside1/raw/41dd2e027f69cc208128a9b60b0a56592ea778bc/doc/HillsideRacing1_Document.pdf)
* [See class design PDF](http://bytebucket.org/knygard/hillside1/raw/41dd2e027f69cc208128a9b60b0a56592ea778bc/doc/HillsideRacing1_ClassStructure.pdf)
* [See gameplay video](http://www.youtube.com/watch?v=aGvb6A62cfM)

## Repository structure

	plan/         Project plan
	doc/          Project documentation
	src/          Source code
	include/      Header files
	res/          Resource files

## Screenshots

![Main menu](http://bytebucket.org/knygard/hillside1/raw/bc6d9b4289cdbdb0e9f4978c057bacaae590b8f4/screenshots/main-menu.png)

![](http://bytebucket.org/knygard/hillside1/raw/bc6d9b4289cdbdb0e9f4978c057bacaae590b8f4/screenshots/racing-1.jpg)

![](http://bytebucket.org/knygard/hillside1/raw/bc6d9b4289cdbdb0e9f4978c057bacaae590b8f4/screenshots/racing-2.jpg)

![](http://bytebucket.org/knygard/hillside1/raw/bc6d9b4289cdbdb0e9f4978c057bacaae590b8f4/screenshots/racing-3.jpg)

![Alpha version](http://bytebucket.org/knygard/hillside1/raw/bc6d9b4289cdbdb0e9f4978c057bacaae590b8f4/screenshots/alpha.png)

## Build

### Dependencies

The game was developed utiling Box2D physics library and SFML multimedia library.

* Box2D 2.2.1
* SFML2.1

### Build instructions

See project documentation.