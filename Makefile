# Compiler and linker
CC=g++-4.7
LD=g++-4.7

# directory definitions
BUILD=bin
SRC=src
RES=res

# SFML (version 2.1)
SFML := $(HOME)/SFML-2.1

# Box2D (version 2.3.0)
#BOX2D := $(HOME)/v2.3.0
BOX2D := $(HOME)/Box2D_v2.2.1

# Read system name to determine linker settings
UNAME:=$(shell uname)

# compiler flags
CFLAGS:=-c -std=c++0x -pedantic -Wextra -g
#-Wall

# Linux, others
#LDFLAGS:=-Wl,-rpath=$(SFML)/lib -lsfml-graphics -lsfml-window -lsfml-audio -lsfml-system
LDFLAGS := -L$(SFML)/lib -lsfml-graphics -lsfml-window -lsfml-system
LDFLAGS += -L$(BOX2D)/Build/Box2D -lBox2D
LIBDIR:= $(SFML)/lib
INCDIR:=-I $(SFML)/include
INCDIR += -I$(BOX2D)

# define sources and targets
SOURCES:=$(wildcard $(SRC)/*.cpp)
OBJECTS:=$(addprefix $(BUILD)/,$(notdir $(SOURCES:.cpp=.o)))
EXECUTABLE:=hillside

# default target
all: clean $(BUILD) $(SOURCES) $(EXECUTABLE)

# target for debug mode
debug: CFLAGS += -DDEBUGGG=1 -g
debug: $(BUILD) $(SOURCES) $(EXECUTABLE)

# create build directory if it does not exist
$(BUILD):
	mkdir -p $@
	cp $(RES)/*/* $@

# Link objects and libraries
$(EXECUTABLE): $(OBJECTS)
	$(LD) $(OBJECTS) $(LDFLAGS) -L $(LIBDIR) -o $(BUILD)/$@ 

# object compilation
$(BUILD)/%.o:$(SRC)/%.cpp 
	$(CC) $(CFLAGS) $(INCDIR) $< -o $@ 

run:
	cd $(BUILD) && export LD_LIBRARY_PATH=$(LIBDIR) && ./$(EXECUTABLE)

clean:
	rm -rf $(BUILD)
