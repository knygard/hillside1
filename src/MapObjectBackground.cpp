#include "../include/MapObjectBackground.hpp"
#include <sstream>

MapObjectBackground::MapObjectBackground(sf::Texture* background, sf::Vector2<float> offsets, sf::Vector2<float> sizes,int type) : MapObject(type), offsets(offsets), sizes(sizes), background(background) {
    bg.setPosition(0,0);
    bg.setTexture(background, false);
}

void MapObjectBackground::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  sf::Vector2<float> vsize = target.getView().getSize();
  sf::Vector2<float> vcenter = target.getView().getCenter();
  float offsetx = offsets.x;   // Offset to adjust location of zero point of background
  float offsety = offsets.y;   // These should be adjusted in the map file
  float texx = -vsize.x/2;     // Offset to adjust the texture zero point
  float texy = -vsize.y/2;     // Do not adjust these. These offset the texture to begin from top left of the screen instead of center 
  float bgw = vsize.x*sizes.x; // Width and height of the texture
  float bgh = vsize.y*sizes.y; // width and the height of the background picture.These should be adjusted on map file
  float bgx=texx-offsetx;      // Position calculation when not near borders (offset the picture to top-left corner and substract the offset)
  float bgy=texy-offsety;      // These shouldn't be changed
  
  sf::RectangleShape bg = this->bg;
  bg.setSize(sf::Vector2<float>(bgw,bgh));
  
  // Handle the cases if the player goes over the background left or top edge (stop the scrolling)
  if (vcenter.x<-offsetx)
    bgx = texx+vcenter.x;
  if (vcenter.y<-offsety)
    bgy = texy+vcenter.y;
  
  // And right and bottom edges
  if (vcenter.x>bgw-vsize.x-offsetx)
    bgx = texx-bgw+vsize.x+vcenter.x;
  if (vcenter.y>bgh-vsize.y-offsety)
    bgy = texy-bgh+vsize.y+vcenter.y;
  
  bg.setPosition(bgx,bgy);
  target.draw(bg);
}

void MapObjectBackground::update() {}

std::string MapObjectBackground::getAsText() const {
  std::stringstream s;
  std::string texname = "bg";
  if (type==MAPTYPE_FRONT)
    texname="fg";
  s << "background" << " "
    << type << " "
    << texname << std::endl;
  return s.str();
}

