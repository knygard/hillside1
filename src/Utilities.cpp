#include "../include/Utilities.hpp"

namespace B2toSFRenderer {
  sf::ConvexShape PolygonToSFConvex(b2PolygonShape& polygonShape)
  {
    sf::ConvexShape shape;
    int32 vcount = polygonShape.GetVertexCount();
    shape.setPointCount(vcount);
    for (int32 i = 0; i < vcount; i++) {
      const b2Vec2 v = polygonShape.GetVertex(i);
      shape.setPoint(i, sf::Vector2f(v.x*PIX_PER_M,
                                     v.y*PIX_PER_M*(-1)));
    }
    return shape;
  }

  sf::CircleShape CircleToSFCircle(b2CircleShape& circleShape)
  {
    sf::CircleShape shape;
    float32 radius = circleShape.m_radius;
    shape.setPointCount(16);
    shape.setRadius(radius * PIX_PER_M);
    shape.setOrigin(radius * PIX_PER_M, radius * PIX_PER_M);
    return shape;
  }
}