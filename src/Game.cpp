#include "../include/Game.hpp"

Game::Game()
{
  timeSinceLastUpdate = sf::Time::Zero;  // for deltatime  
}

Game::~Game()
{
  for(auto iter = players.begin(); iter != players.end(); iter++)
    delete *iter; 	
}
  
void Game::resetState(int pl1vehicle, std::string m) {
  players.clear();
  map = new Map(m);
  players.push_back(new Player(0, getVehicle(pl1vehicle), map));
  //if(player_number == 2) {players.push_back(new Player(0, getVehicle(pl2vehicle), map));} //multiplayer
}
  
void Game::resetState() {
  players.clear();
}

int Game::run(sf::RenderWindow& window, sf::Event& event)
{
  sf::Time restart = clock.restart();
  timeSinceLastUpdate += restart;
  
  while (timeSinceLastUpdate > sf::seconds(timeStep))
    {
      timeSinceLastUpdate -= sf::seconds(timeStep);
      map->getWorld()->Step(timeStep, velocityIterations, positionIterations);
    }
  
  for (auto player : players) {
    player->processEvent(event, timeSinceLastUpdate.asSeconds());
    player->update(); // Updates only view position and ui texts
  }
  
  map->destroyCollectables();
  
  window.draw(*this);
  
  return GAMESTATE_GAME;
}

void Game::setMap(size_t m) {mapnum = m;}

std::vector<Player*> Game::getPlayers() {
  return players;
}
  
MapObjectVehicle* Game::getVehicle(int id) {
  if (id == 0)
    return new TurboTruck(map->getWorld());
  else if (id == 1)
    return new HyperCab(map->getWorld());
  else
    return new RamboTractor(map->getWorld());
}

// This is separated so that it can be called while in menu
void Game::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.setView(rt.getDefaultView());
  for (Player* p : players)
    rt.draw(*p);
}
