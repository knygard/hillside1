#include "../include/MapObjectItem.hpp"

MapObjectItem::MapObjectItem(std::string texname, int type, std::istringstream& s) : MapObject(type), texname(texname), t(NULL) {
  float x,y;
  
  s >> x >> y;
  shape.setPosition(x*20*2,-y*20*2);
  s >> x >> y;
  shape.setSize(sf::Vector2<float>(x,y));
}

MapObjectItem::MapObjectItem(std::string texname, int type, float x, float y, float w, float h) : MapObject(type), texname(texname), t(NULL) {

  shape.setPosition(x*20*2,-y*20*2);
  shape.setSize(sf::Vector2<float>(w,h));
}

void MapObjectItem::updateTextures(std::map<std::string, sf::Texture*>& textures) {
  t = textures[texname];
  shape.setTexture(t);
}

void MapObjectItem::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(shape);
}

std::string MapObjectItem::getAsText() const {
  std::stringstream s;
  s << "item" << " "
    << texname << " " 
    << type << " "
    << shape.getPosition().x/40 << " "
    << -shape.getPosition().y/40 << " "
    << shape.getSize().x << " "
    << shape.getSize().y << std::endl;
  return s.str();
}
