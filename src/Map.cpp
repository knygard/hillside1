#include "../include/Map.hpp"

Map::Map() {
  filename="";
  // Initialize the world
  b2Vec2 gravity(0, -9.8);
  world = new b2World(gravity);
  world->SetAllowSleeping(true);
  world->SetContactListener(&cl);
}

const std::vector<MapObject*>* Map::getObjects() const {
  return &objects;
}

std::map<std::string, sf::Texture*>& Map::getTextures() {
  return textures;
}
 
const std::map<std::string, std::string>* Map::getTextureFilenames() const {
  return &textures_fname;
}

void Map::saveMap(std::string mapfilename) {
  std::ofstream o(mapfilename);
  o << *this;
  filename=mapfilename;
}

Map::~Map()
{
  delete world;
}

sf::Texture* Map::loadTexture(std::string id, std::string fname) {
  if (textures[id]==NULL)
    textures[id] = new sf::Texture();
  if (fname!="") {
    textures[id]->loadFromFile(fname);
    textures_fname[id] = fname;
  }
  return textures[id];
}

Map::Map(std::string mapfilename) : Map() {
  filename = mapfilename;
  std::ifstream in;       // String for reading file
  std::string type;       // identifier of data
  std::string fname;      // temporary storage for texture filename
  MapObject* obj;         // temporary storage for new MapObjects
  std::string id;         // temporary variable for MapObject type (which layer the data will be drawn)
  sf::Texture* t;         // temporary texture storage
  char cstr[4096] = { 0 };// c string to store one line from input (used by std::istringstream) and contains one data entry
  
  // Reads lines from file and checks the identifier (first word on line) and takes an action related to ir
  // level1.txt contains also descriptions about the datafields
  in.open(filename);
  while ((in.rdstate() & std::ifstream::failbit ) == 0 ) {
    in.getline(cstr,4096);
    if ((in.rdstate() & std::ifstream::failbit ) != 0 ) break;
    std::istringstream in_line(cstr);
    in_line >> type;
    
    // Load new texture
    if (type=="texture") {
      //t = new sf::Texture();
      in_line >> id >> fname;
      //std::cout << fname << "." << id << std::endl;
      textures_fname[id] = fname;
      t = loadTexture(id);
      //t->loadFromFile(fname);
      t->loadFromFile(fname);
    }
    
    // Create MapObjectBackground object
    if (type=="background2") {
      in_line >> id;
      t = loadTexture(id);
      obj = new MapObjectBackground(t, sf::Vector2<float>(100,300), sf::Vector2<float>(2,1.5));
      objects.push_back(obj);
    }
    
    if (type=="background") {
      int type_id;
      in_line >> type_id >> id;
      t = loadTexture(id);
      if (type_id==MAPTYPE_SKY) {
	obj = new MapObjectBackground(t, sf::Vector2<float>(100,300), sf::Vector2<float>(2,1.5));
      } else { 
	obj = new MapObjectBackground(t, sf::Vector2<float>(100,-1000), sf::Vector2<float>(20,2), MAPTYPE_FRONT);
      }
      objects.push_back(obj);
    }
    
    // Create MapObjectBackground object which will be shown on foreground
    if (type=="foreground2") {
      in_line >> id;
      t = loadTexture(id);
      obj = new MapObjectBackground(t, sf::Vector2<float>(100,-1000), sf::Vector2<float>(20,2), MAPTYPE_FRONT);
      objects.push_back(obj);
    }
    
    // Load shape which represents the actual terrain (or when passable it can be for example cave wall)
    if (type=="triangles") {
      int impassable;
      int type_id;
      in_line >> id >> type_id >> impassable;
      t = loadTexture(id);
      obj = new MapObjectTriangles(world, id, (impassable ? false : true), in_line, type_id);
      objects.push_back(obj);
    }
    
    // Loads decorational item (for example a bridge) from file
    if (type=="item") {
      in_line >> id;
      int type_id;
      in_line >> type_id;
      t = loadTexture(id);
      obj = new MapObjectItem(id, type_id, in_line);
      objects.push_back(obj);
    }
    
    // Load collectable coin from file
    if (type=="coin") {
      obj = new MapObjectCoin(world, in_line);
      objects.push_back(obj);
    }
    
    if (type=="boost") {
      obj = new MapObjectBoost(world, in_line);
      objects.push_back(obj);
    }
    
    if (type=="goal") {
      obj = new MapObjectGoal(world, in_line);
      objects.push_back(obj);
    }
    
    if (type=="startpoint") {
      float nx,ny;
      in_line >> nx;
      playerStartPoint.x = nx;
      in_line >> ny;
      playerStartPoint.y = ny;
      obj = new MapObjectStartpoint(nx,ny);

      objects.push_back(obj);
    }
  }

  for (MapObject* m : objects) {
    m->updateTextures(textures);
  }
}

b2World* Map::getWorld() {
  return world;
}
  
std::string Map::getMapfilename() {
  return filename;
}

// Add new object to world
void Map::addObject(MapObject* m) {
  objects.push_back(m);
}

void Map::destroyCollectables() {
  std::vector<MapObjectCollectable*> toBeDestroyed;
  for (auto it = objects.begin(); it != objects.end(); it++) {
    if((*it)->isCollectable() && (*it)->isDestroyed())
      toBeDestroyed.push_back(static_cast<MapObjectCollectable*>(*it));
  }
  for (auto it = objects.begin(); it != objects.end(); it++) {
    if((*it)->isCollectable() && (*it)->isDestroyed())
      it = objects.erase(it);
  }
  for (MapObjectCollectable* c : toBeDestroyed)
    c->destroy();
}


/* Update map item positions  */
void Map::update() {
  for (MapObject* m : objects) {
    m->update();
  }
}

// Draws some certain type of items to view. The type indicates the layer where the object will be drawn.
void Map::draw(sf::RenderTarget &target, sf::RenderStates states, int typemask) const {
  for (MapObject* m : objects)
    if (m->getType()&typemask)
      target.draw(*m);
}

std::ostream& operator<<(std::ostream& os, const Map& m) {
  
  for (std::pair<std::string, std::string> p : *m.getTextureFilenames()) {
    os << "texture " << p.first << " " << p.second << std::endl;
  }
  
  for (MapObject* mo : *m.getObjects()) {
    if (mo!=NULL)
      os << *mo;
  }
  return os;
} 

