#include "../include/RamboTractor.hpp"

RamboTractor::RamboTractor(b2World* world) : TwoWheelVehicle(world)
{
  /* Parameters */
  {
    maxSpeed = 12.0f;
    maxMotorSpeed = 10.0f;
    angularImpulse = 250.0f;
    boostFactor = 1.8;
    acceleration = 3.0f;
    driveType = RearWheelDrive;
  }

  /* Chassis */
  {
    chassisTexturePath = "RamboTractor_chassis.png";

    chassis_distance = -0.7f;
    chassisVertices[0].Set(-4.0f, -0.0f);
    chassisVertices[1].Set(4.0f, -0.0f);
    chassisVertices[2].Set(3.8f, 2.3f);
    chassisVertices[3].Set(0.4f, 4.9f);
    chassisVertices[4].Set(-3.3f, 4.9f);
    chassisVertices[5].Set(-4.0f, 3.4f);

    // Material
    chassisFixture.density = 0.3f;
    chassisFixture.friction = 0.5f;
    chassisFixture.restitution = 0.1;
  }

  /* Wheels */
  {
    wheelTexturePath = "tire.png";
    rearWheelRadius = 2.0f;
    frontWheelRadius = 1.3f;

    rearWheel_position.Set(-2.5f, 0.0f);
    frontWheel_position.Set(3.4f, -1.0f);

    // Motors (joints)

    rearWheelJoint.maxMotorTorque = 850.0f;
    rearWheelJoint.frequencyHz = 8;
    rearWheelJoint.dampingRatio = 0.2;

    frontWheelJoint.maxMotorTorque = 0.0f;
    frontWheelJoint.frequencyHz = 12;
    frontWheelJoint.dampingRatio = 0.2;

    // Materials

    rearWheelFixture.density = 0.4f;
    rearWheelFixture.friction = 3.5f;
    rearWheelFixture.restitution = 1.0f;

    frontWheelFixture.density = 0.6f;
    frontWheelFixture.friction = 0.8f;
    frontWheelFixture.restitution = 0.7f;
  }

  // Initialization !important
  initialize();
}

void RamboTractor::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(sfChassis);
  rt.draw(sfFrontWheel);
  rt.draw(sfRearWheel);
}