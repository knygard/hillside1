#include "../include/MenuEdit.hpp"

void MenuEditEnumerator::menuEvent(MenuChoice& who) {
  currentSelection = (currentSelection+1)%values.size();
  who.setText(this->getText());
}

std::string MenuEditEnumerator::getText() {
  std::stringstream s;
  s << name << ": " << values[currentSelection];
  return s.str();
}

int MenuEditEnumerator::getCurrentSelection() {
  return currentSelection;
}

void MenuEditEnumerator::addChoice(std::string choice) {
  values.push_back(choice);
}

MenuEdit::MenuEdit(Editor& e) : Menu("Editor", GAMESTATE_EDITMENU, NULL), e(e),
				theme("Theme (if new)"), map("Map"), edit(this) {
  std::ifstream in;
  char tmp[256];
  
  theme.addChoice("Mountains");
  theme.addChoice("Canyon");
  theme.addChoice("England");
  addItem(theme.getText(), -1, false, &theme, 0);
  
  in.open("maps.txt");
  while(1) {
    in.getline(tmp, 256);
    if ((in.rdstate() & std::ifstream::failbit ) != 0 ) break;
    maplist.push_back(tmp);
    in.getline(tmp, 256);
    if ((in.rdstate() & std::ifstream::failbit ) != 0 ) break;
    map.addChoice(tmp);
  }
  maplist.push_back("NEWMAP"); // Meaning new map
  map.addChoice("<New map>");
  addItem(map.getText(), -1, false, &map, 0);

  addItem("Edit", GAMESTATE_EDITOR, true, &edit, 0); 
}

std::string MenuEdit::getMap() {
  return maplist[map.getCurrentSelection()];
}

int MenuEdit::getTheme() {
  return theme.getCurrentSelection();
}

Editor& MenuEdit::getEditor() {
  return e;
}


void MenuEditEdit::menuEvent(MenuChoice& who) {
  owner->getEditor().load(owner->getMap(), owner->getTheme());
}
