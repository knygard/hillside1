#include "../include/MapObjectCoin.hpp"

MapObjectCoin::MapObjectCoin(b2World* world, std::istringstream& s) : MapObjectCollectable(world, s, MAPOBJ_IDENT_COIN) {
  init();
}

MapObjectCoin::MapObjectCoin(b2World* world, float x, float y) : MapObjectCollectable(world, x,y, MAPOBJ_IDENT_COIN) {
  init();
}

void MapObjectCoin::init() {
  myShape.m_radius = 1.0f;
  coin = B2toSFRenderer::CircleToSFCircle(myShape);
  cointexture.loadFromFile("coin.png");
  cointexture.setSmooth(true);
  coin.setTexture(&cointexture, true);
  coin.setOrigin(coin.getRadius(), coin.getRadius());
  coin.setPosition(x*20*2,-y*20*2);
  addToWorld();
}

void MapObjectCoin::draw(sf::RenderTarget &rt, sf::RenderStates states) const
{
  if(!collected) {rt.draw(coin);}
}

std::string MapObjectCoin::getName() const { return "coin"; }
b2Shape& MapObjectCoin::getShape() { return myShape; }

