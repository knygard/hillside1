#include "../include/Player.hpp"

/*
 * PlayerHud
 */
PlayerHud::PlayerHud()
{
  uifont.loadFromFile("whitrabt.ttf");
  padding = 20.0f;
  float offset = 30.0f;

  // Set initial parameters for points text
  points_text.setFont(uifont);
  points_text.setCharacterSize(30);
  points_text.setColor(sf::Color::White);
  points_text.setPosition(padding, padding-10);

  // Set initial parameters for boost text
  boosts_text.setFont(uifont);
  boosts_text.setCharacterSize(30);
  boosts_text.setColor(sf::Color::White);
  boosts_text.setPosition(padding, padding-10+offset);


  // Set initial parameters for speed text
  speed_text.setFont(uifont);
  speed_text.setCharacterSize(30);
  speed_text.setColor(sf::Color::White);
  speed_text.setPosition(padding-3.0f, padding-10+2*offset);

  // Set initial parameters for time text
  time_text.setFont(uifont);
  time_text.setCharacterSize(40);
  time_text.setStyle(sf::Text::Bold);
  time_text.setColor(sf::Color::White);
  time_text.setPosition(800, 10);
}

void PlayerHud::update(int points, int boosts, int speed) {
  points_ss.str("");
  points_ss << "POINTS: " << points;
  points_text.setString(points_ss.str());

  speed_ss.str("");
  speed_ss << " SPEED: " << std::fixed << std::setprecision(0) << std::abs(speed);
  speed_text.setString(speed_ss.str());

  boosts_ss.str("");
  boosts_ss << "BOOSTS: " << std::fixed << std::setprecision(0) << boosts;
  boosts_text.setString(boosts_ss.str());

  time_ss.str("");
  std::string t_min_zero;
  std::string t_s_zero;
  std::string t_cs_zero;
  size_t t_ms = clock.getElapsedTime().asMilliseconds();
  size_t t_min = t_ms/60000;
  if(t_min < 10) {t_min_zero = "0";} else {t_min_zero = "";}
  size_t t_s = t_ms/1000 - t_min*60;
  if(t_s < 10) {t_s_zero = "0";} else {t_s_zero = "";}
  size_t t_cs = t_ms/10 - t_min*6000 - t_s*100;
  if(t_cs < 10) {t_cs_zero = "0";} else {t_cs_zero = "";}
  time_ss << std::fixed << std::setprecision(0) << t_min_zero << t_min << ":" << t_s_zero << t_s << ":" << t_cs_zero << t_cs;
  time_text.setString(time_ss.str());
}

void PlayerHud::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  target.draw(points_text);
  target.draw(boosts_text);
  target.draw(speed_text);
  target.draw(time_text);
}


/*
 * Player
 */
Player::Player(int id, MapObjectVehicle* vehicle, Map* map) :
  map(map), vehicle(vehicle), defaultx(0), defaulty(0), keyWasPressed(false)
  {
    hud = new PlayerHud();
    skyView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
    staticView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
    gameView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
    frontView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
    map->addObject(vehicle); // add vehicle to map's objects
    setPosition(map->getPlayerStartPoint().x, map->getPlayerStartPoint().y);
  }

Player::~Player()
{
  delete vehicle;
  delete map;
}

void Player::update() {
  // Update view position to match the player or if no player exists then use static coordinates
  float posx, posy;
  if (&vehicle != NULL) {
    posx = vehicle->getPosition().x*20+100;
    posy = -vehicle->getPosition().y*20-150;
  } else {
    posx=defaultx;
    posy=defaulty;
  }
  gameView.setCenter(posx, posy);
  skyView.setCenter(posx*0.2, posy*0.2);
  frontView.setCenter(posx*1.05, posy*1.05);
  map->update();
  hud->update(getPoints(),
              getBoosts(),
              abs(vehicle->getVelocity().x) + abs(vehicle->getVelocity().y));
}

void Player::setPosition(float x, float y) {
  vehicle->setPosition(x, y);
}

void Player::zoom(float amount) {
  gameView.zoom(amount);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  sf::ConvexShape sh;
  sh.setPosition(0,0);
  sh.setPointCount(3);
  sh.setPoint(0,sf::Vector2f(0,0));
  sh.setPoint(1,sf::Vector2f(1,0));
  sh.setPoint(2,sf::Vector2f(0,1));

  target.setView(skyView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_SKY);    // Draw sky behind the playfield

  target.setView(gameView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_BG);     // Draws everything behind player
  map->draw(target, states, MAPTYPE_PLAYER); // Draw the player to playfield
  map->draw(target, states, MAPTYPE_FG);     // Draws everything in front of player

  target.setView(frontView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_FRONT);  // Draw Decorations in front of the playfield

  target.setView(staticView);
  target.draw(sh);
  target.draw(*hud);                         // Draw heads up display
}

void Player::processEvent(sf::Event event, float deltaTime) {

  if (event.type == sf::Event::KeyReleased) {
    vehicle->idle();
    keyWasPressed = false;
  } else {
    keyboard(deltaTime);
  }
}

void Player::keyboard(float deltaTime) {

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::R) && !keyWasPressed) {
    vehicle->setPosition(map->getPlayerStartPoint().x, map->getPlayerStartPoint().y);
    keyWasPressed = true;
    return;
  }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !keyWasPressed) {
    vehicle->tilt(false);
  } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !keyWasPressed) {
    vehicle->tilt(true);
  }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && !keyWasPressed) {
    vehicle->boost();
  }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
    vehicle->forward(deltaTime);
  }
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
    vehicle->backward(deltaTime);
  }

  keyWasPressed = true; // cleared in processEvents
}

  /* an algorithm that seaches the correct position of the result (ascending order)
   * returns the position of the result (ranking) 
  */

  size_t Player::getRanking(std::string fname)
  {
    ResultLoader rl;
    size_t time = this->getTime();
    size_t points = this->getPoints();
    std::vector<Result> results = rl.getResults(fname);
    Result res("", time, points);
    int game_points = res.calculateGamePoints();
    size_t ranking = 0;
    if(results.size() == 0) {
      ranking++;
      return ranking;
    } else {
      for(auto it = results.begin(); it != results.end();)
      {
        ranking++;
        if(game_points > it->calculateGamePoints()) {
          it++;
          if(it == results.end()) {
            break;}
        } else {
          break;
        }
      }
    }
    return ranking;
  }
  
  /* an algorithm that seaches the correct position of the result (ascending order)
   * writes the result to the resultfile
  */
  
  void Player::saveResult(std::string fname, std::string name, size_t points, size_t time)
  {
    ResultLoader rl;
    std::vector<Result> results = rl.getResults(fname);
    Result res(name, time, points);
    int game_points = res.calculateGamePoints();
    if(results.size() == 0) {
      results.push_back(res);
    } else {
      for(auto it = results.begin(); it != results.end();)
      {
        if(game_points > it->calculateGamePoints()) {
          it++;
          if(it == results.end()) {
            results.push_back(res);
            break;
          }
        } else {
          it = results.insert(it, res);
          break;
        }
      }
    }
    std::ofstream resultfile;
    std::stringstream ss;
    ss << "Results_" << fname;
    resultfile.open(ss.str());
    for(auto it = results.begin(); it != results.end(); it++)
    {
      resultfile << it->getName() << " " << it->getTime() << " " << it->getPoints() << "\n";
    }
    resultfile.close();
  } 

