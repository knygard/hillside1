#include "../include/EditorMenu.hpp"

////////// EditorButton //////////////

EditorButton::EditorButton(std::string filename, std::string name, int id, int x, int y, bool subpic) : name(name) , id(id), subpic(subpic) {
  image.loadFromFile(filename);
  image.setSmooth(true);
  shape.setTexture(&image);
  shape.setSize(sf::Vector2<float>(60,60));
  shape.setPosition(x,y);
    
  if (subpic) {
    shape2.setSize(sf::Vector2<float>(50,50));
    shape2.setPosition(x+5,y+5);
  }
}

void EditorButton::setTexture2(sf::Texture* tex) {
  shape2.setTexture(tex, true);
}

void EditorButton::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(shape);
  if (subpic)
    rt.draw(shape2);
}

std::string& EditorButton::getName() { return name; }
int EditorButton::getId() { return id; }

////////// EditorFB //////////////

EditorFB::EditorFB(int x, int y) {
  front.loadFromFile("btn_bg.png");
  back.loadFromFile("btn_fg.png");
  front.setSmooth(true);
  back.setSmooth(true);
  shape.setTexture(&front);
  shape.setSize(sf::Vector2<float>(60,60));
  shape.setPosition(x,y);
}

void EditorFB::setState(int newstate) {
  state = newstate;
  if (state==1)
    shape.setTexture(&back);
  else
    shape.setTexture(&front);
}

void EditorFB::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(shape);
}

/////////// EditorMenu ////////////

EditorMenu::EditorMenu()
{
  uifont.loadFromFile("sansation.ttf");
  
  // Create the buttons and add them to this menu
  buttons.push_back(new EditorButton("btn_boost.png"        ,
				     "Add boost"        , EDBTN_BOOST , buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_coin.png"         ,
				     "Add coin"         , EDBTN_COIN , buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_goal.png"         ,
				     "Set goal"         , EDBTN_GOAL , buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_p1.png"           ,
				     "Add P1 startpoint", EDBTN_PL1 , buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_random_item.png"  ,
				     "Random item"      , EDBTN_RANDOM, buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_save.png"         ,
				     "Save map"         , EDBTN_SAVE, buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_triangle.png"     ,
				     "Add Triangle"     , EDBTN_TRIANGLES, buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_trianglefan.png"  ,
				     "Add Trianglefan"  , EDBTN_TRIANGLEFAN, buttons.size()*ED_BS+BTN_OFFSET, 0));
  buttons.push_back(new EditorButton("btn_trianglestrip.png",
				     "Add Trianglestrip", EDBTN_TRIANGLESTRIP, buttons.size()*ED_BS+BTN_OFFSET, 0));
  
  /* Set initial parameters for desctription text */
  desc.setFont(uifont);
  desc.setCharacterSize(25);
  desc.setStyle(sf::Text::Bold);
  desc.setColor(sf::Color::White);
  desc.setPosition(0,0);
  if (selection!=-1)
    desc.setString(buttons[selection]->getName());
  else
    desc.setString("None");

  /* Set initial parameter for coordinates text*/
  coords.setFont(uifont);
  coords.setCharacterSize(25);
  coords.setStyle(sf::Text::Bold);
  coords.setColor(sf::Color::White);
  coords.setPosition(0,25);
  coords.setString("0, 0");
  
  /* Load and setup selection frame */
  selected.loadFromFile("btn_selection.png");
  sel.setTexture(&selected,true);
  sel.setSize(sf::Vector2<float>(60,60));
  sel.setPosition(selection*ED_BS+BTN_OFFSET,0);

  /* Load and setup hilight frame */  
  hilighted.loadFromFile("btn_hilight.png");
  hil.setTexture(&hilighted,true);
  hil.setSize(sf::Vector2<float>(60,60));
    hil.setPosition(selection*ED_BS+BTN_OFFSET,0);
}

void EditorMenu::update(int x, int y, bool activate) {
  
  std::stringstream s;
  s << x << ", " << y;
  coords.setString(s.str());
  
  // If cursor y position is less than editor button size (on top of buttons)
  if (y<ED_BS) {
    // No iterator is used because no elements are really iterated through
    for (size_t i=0;i<buttons.size();i++) {
      if ((((size_t)x>ED_BS*i+BTN_OFFSET) && ((size_t)x<ED_BS*(i+1)+BTN_OFFSET))) {
	setHilight(i);
	if (activate&&!activated) {
	  activated=true;
	  setSelection(i);
	  desc.setString(buttons[selection]->getName());
	}
      }
    }
  } else {
    hilight = -1;
  }
  
  if (!activate)
    activated = false;
  
}

void EditorMenu::setDesc(std::string newdesc) {
  desc.setString(newdesc);
}

void EditorMenu::setSelection(int s) {
  selection = s;
  if (selection!=-1)
    sel.setPosition(selection*ED_BS+BTN_OFFSET,0);
}

int EditorMenu::getSelection() {
  return selection;
}

int EditorMenu::getId() {
  if (selection!=-1)
    return buttons[selection]->getId();
  else
    return -1;
}

void EditorMenu::setHilight(int h) {
  hilight = h;
  hil.setPosition(hilight*ED_BS+BTN_OFFSET,0);
}

int EditorMenu::getHilight() {
  return hilight;
}

// Draw the contents
void EditorMenu::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  target.draw(desc);
  target.draw(coords);
  for (EditorButton* b : buttons)
    target.draw(*b);
  if (selection!=-1)
    target.draw(sel);
  if (hilight!=-1)
    target.draw(hil);
}

////////// EditorItems ////////////

EditorItems::EditorItems()
{
  uifont.loadFromFile("sansation.ttf");
  
  loadItems();
  
  int i=9;
  buttons.push_back(new EditorButton("btn_prev.png","",0, (buttons.size()+2)*ED_BS, WINDOW_HEIGHT-ED_BS));
  while (i--)
    buttons.push_back(new EditorButton("btn_empty.png","",0, (buttons.size()+2)*ED_BS, WINDOW_HEIGHT-ED_BS, true));
  buttons.push_back(new EditorButton("btn_next.png","",0, (buttons.size()+2)*ED_BS, WINDOW_HEIGHT-ED_BS));
  
  selected.loadFromFile("btn_selection.png");
  sel.setTexture(&selected,true);
  sel.setSize(sf::Vector2<float>(60,60));
  sel.setPosition(selection*ED_BS+ED_BS*2,WINDOW_HEIGHT-ED_BS);
  
  hilighted.loadFromFile("btn_hilight.png");
  hil.setTexture(&hilighted,true);
  hil.setSize(sf::Vector2<float>(60,60));
  hil.setPosition(selection*ED_BS+ED_BS*2,WINDOW_HEIGHT-ED_BS);  
}

std::vector<EditorItem*>& EditorItems::getItems() {
  return items;
}

void EditorItems::loadItems() {
  std::ifstream i("decorations.txt");
  sf::Texture* tex;
  std::string texname;
  std::string filename;
  float w, h;
  while(1) {
    i >> filename >> texname >> w >> h;
    if ((i.rdstate() & std::ifstream::failbit ) != 0 ) break;
    tex = new sf::Texture();
    tex->loadFromFile(filename);      
    items.push_back(new EditorItem(tex, texname, filename, sf::Vector2f(w,h)));
  }
}

void EditorItems::update(int x, int y, bool activate) {
  
  for (size_t i=1;i<buttons.size()-1;i++) {
    if (i+offset<items.size())
      buttons[i]->setTexture2(items[i+offset]->tex);
    else 
      buttons[i]->setTexture2(NULL);
  }
  
  // If cursor y position is less than editor button size (on top of buttons)
  if (y>WINDOW_HEIGHT-ED_BS) {
    // No iterator is used because no elements are really iterated through
    for (size_t i=0;i<buttons.size();i++) {
      if ((((size_t)x>ED_BS*i+ED_BS*2) && ((size_t)x<ED_BS*(i+1)+ED_BS*2))) {
	setHilight(i);
	if (activate&&!wasActivated) {
	  if (i==0) {
	    if (offset>=0)
	      offset--;
	  } else if (i==buttons.size()-1) {
	    if (offset+buttons.size()<=items.size())
	      offset++;
	  }
	  else
	    setSelection(i);
	  wasActivated=true;
	}
	if (!activate)
	  wasActivated=false;
      }
    }
  } else {
    hilight = -1;
  }
}

void EditorItems::setSelection(int s) {
  selection = s;
  if (selection!=-1)
    sel.setPosition(selection*ED_BS+ED_BS*2,WINDOW_HEIGHT-ED_BS);
}

int EditorItems::getSelection() {
  return selection;
}

int EditorItems::getId() {
  if (selection==-1)
    return -1;
  return selection+offset;
}

void EditorItems::setHilight(int h) {
  hilight = h;
  hil.setPosition(hilight*ED_BS+ED_BS*2,WINDOW_HEIGHT-ED_BS);
}

int EditorItems::getHilight() {
  return hilight;
}

EditorItem* EditorItems::getItem(int item) {
  return items[item];
}

void EditorItems::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  for (EditorButton* b : buttons)
    target.draw(*b);
  if (selection!=-1)
    target.draw(sel);
  if (hilight!=-1)
    target.draw(hil);
}

