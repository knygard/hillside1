#include "../include/MenuNewGame.hpp"

void MenuNewGameEnumerator::menuEvent(MenuChoice& who) {
  currentSelection = (currentSelection+1)%values.size();
  who.setText(this->getText());
}

std::string MenuNewGameEnumerator::getText() {
  std::stringstream s;
  s << name << ": " << values[currentSelection];
  return s.str();
}

int MenuNewGameEnumerator::getCurrentSelection() {
  return currentSelection;
}

void MenuNewGameEnumerator::addChoice(std::string choice) {
  values.push_back(choice);
}

MenuNewGame::MenuNewGame(Game& g) : Menu("New game", GAMESTATE_MAPSELECTION, &g), g(g), 
				    pl1car("Vehicle"), map("Map"), startgame(this, g) {
  std::ifstream in;

// could be used for the multiplayer
//  players.addChoice("1");
//  players.addChoice("2");
//  addItem(players.getText(), -1, false, &players, 0);

  pl1car.addChoice("Turbo Truck");
  pl1car.addChoice("Hyper Cab");
  pl1car.addChoice("Rambo Tractor");
  addItem(pl1car.getText(), -1, false, &pl1car, 0);

// could be used for the multiplayer
//  pl2car.addChoice("Turbo Truck");
//  pl2car.addChoice("Hyper Cab");
//  pl2car.addChoice("Rambo Tractor");
//  addItem(pl2car.getText(), -1, false, &pl2car, 0);

  in.open("maps.txt");
  for (auto obj : readLevels(in)) {
    map.addChoice(obj.first);
    maplist.push_back(obj.second);
  }

  addItem(map.getText(), -1, false, &map, 0);

  addItem("GO!", GAMESTATE_GAME, true, &startgame, 0);
}

/**
 * @brief Reads levels from file
 * @param in Input filestream
 * @return Map of levelname-filename pairs
 */
std::map<std::string, std::string> MenuNewGame::readLevels(std::ifstream& in) {
  std::map<std::string, std::string> levels;
  while(1) {
    char filename[256];
    char levelname[256];
    in.getline(filename, 256);
    if ((in.rdstate() & std::ifstream::failbit) != 0 ) break;
    in.getline(levelname, 256);
    if ((in.rdstate() & std::ifstream::failbit) != 0 ) break;
    levels[levelname] = filename;
  }
  return levels;
}

// could be used for the multiplayer
//int MenuNewGame::getPlayers() {
//  return players.getCurrentSelection();
//}

int MenuNewGame::getPl1car() {
  return pl1car.getCurrentSelection();
}

// could be used for the multiplayer
//int MenuNewGame::getPl2car() {
//  return pl2car.getCurrentSelection();
//}

std::string MenuNewGame::getMap() {
  return maplist[map.getCurrentSelection()];
}

void MenuNewGameStartGame::menuEvent(MenuChoice& who) {
  g.resetState(owner->getPl1car(), owner->getMap());
}

