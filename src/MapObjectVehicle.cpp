#include "../include/MapObjectVehicle.hpp"

void MapObjectVehicle::applyAngularImpulse(std::vector<b2Body*> bodies) {
  for (auto body : bodies)
    body->ApplyAngularImpulse(boostAmount*boostFactor*body->GetAngularVelocity() /
                              std::abs(body->GetAngularVelocity()));
}

void MapObjectVehicle::deleteCollected(int ident) {
  for (auto it = collectables.begin(); it != collectables.end(); it++)
    if (*it == ident) {
      it = collectables.erase(it);
      break;
    }
}

void MapObjectVehicle::addCollectable(int ident) {
  collectables.push_back(ident);
}

std::vector<int> MapObjectVehicle::getCollectables(int ident) const {

  std::vector<int> selectedCollectables;
  for (int collectable : collectables)
    if(collectable == ident)
      selectedCollectables.push_back(collectable);
  return selectedCollectables;
}