#include "../include/ScoreManager.hpp"

/* ScoreManager */

ScoreManager::ScoreManager(Game& g) :
Menu(std::string("High Scores"), GAMESTATE_HIGHSCORES, &g), mygame(g)
{ }

void ScoreManager::menuEvent(MenuChoice& who) {
  // nothing
}

void ScoreManager::update(std::string fname) {
  clear();
  std::vector<std::pair<std::string, size_t>> highscores = rl.getHighScores(fname, 3);
  size_t i = 1;
  for (auto hs : highscores) {
    std::stringstream ss;
    ss << i << ". " << hs.first << " " << hs.second;
    addItem(ss.str(), GAMESTATE_HIGHSCORES);
    i++;
  }
}


/* MapManager */

MapManager::MapManager(Game& g, std::string maplist) : Menu("High Scores", GAMESTATE_HIGHSCORES_MAPSELECTION, &g), mygame(g) {
  /* Reading the map filenames and names from a file */
  std::ifstream in;
  char tmp[256];
  int index;
  in.open(maplist);
  index=0;
  while(1) {
    in.getline(tmp, 256);
    if ((in.rdstate() & std::ifstream::failbit ) != 0 ) break;
    maps[index] = tmp;
    in.getline(tmp, 256);
    if ((in.rdstate() & std::ifstream::failbit ) != 0 ) break;
    mapnames[index] = tmp;
    addItem(tmp, GAMESTATE_HIGHSCORES, true, this, index++);
  }
}

void MapManager::menuEvent(MenuChoice& who) {
  fname = maps[who.getId()];
}

