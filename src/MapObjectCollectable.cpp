#include "../include/MapObjectCollectable.hpp"

MapObjectCollectable::MapObjectCollectable(b2World* world, std::istringstream& s, int type) : MapObject(MAPTYPE_FG, type), world(world) {
  collected = false;
  s >> x >> y;
}

MapObjectCollectable::MapObjectCollectable(b2World* world, float nx, float ny, int type) : MapObject(MAPTYPE_FG, type), world(world) {
  collected = false;
  x = nx;
  y = ny;
}

void MapObjectCollectable::destroy() {
  if (world!=NULL)
    body->GetWorld()->DestroyBody(body);
}

void MapObjectCollectable::collect() {
  collected=true;
}

bool MapObjectCollectable::isDestroyed() {return collected;}

void MapObjectCollectable::setPosition(float newx,float newy) {
  x = newx;
  y = newy;
}

std::string MapObjectCollectable::getAsText() const {
  std::stringstream s;
  s << getName() << " "
  << x << " "
  << y << std::endl;
  return s.str();
}

bool MapObjectCollectable::addToWorld() {
  if (world!=NULL) {
    b2FixtureDef fd;
    fd.shape = &getShape();
    fd.density = 1.1f;
    fd.isSensor = true;
    b2BodyDef bd;
    bd.position.Set(x*2,y*2);
    body = world->CreateBody(&bd);
    body->CreateFixture(&fd);
    // This is the data which will be passed to handler in contact to another object
    body->SetUserData(this);
    return true;
  } else {
    return false;
  }
}

void CollectableListener::BeginContact(b2Contact* contact) {
  void* bodyUserData1 = contact->GetFixtureA()->GetBody()->GetUserData();
  void* bodyUserData2 = contact->GetFixtureB()->GetBody()->GetUserData();
  if ( bodyUserData1 && bodyUserData2 )
    if ((static_cast<MapObject*>(bodyUserData1)->isCollectable() &&
         static_cast<MapObject*>(bodyUserData2)->getIdent()==MAPOBJ_IDENT_VEHICLE)) {
      static_cast<MapObjectCollectable*>(bodyUserData1)->collect();
      static_cast<MapObjectVehicle*>(bodyUserData2)->addCollectable(static_cast<MapObject*>(bodyUserData1)->getIdent());
    }

  if ( bodyUserData1 && bodyUserData2 )
    if ((static_cast<MapObject*>(bodyUserData2)->isCollectable() &&
         static_cast<MapObject*>(bodyUserData1)->getIdent()==MAPOBJ_IDENT_VEHICLE)) {
      static_cast<MapObjectCollectable*>(bodyUserData2)->collect();
      static_cast<MapObjectVehicle*>(bodyUserData1)->addCollectable(static_cast<MapObject*>(bodyUserData2)->getIdent());
    }
}

void CollectableListener::EndContact(b2Contact* contact) {
}
