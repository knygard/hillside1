#include "../include/Finish.hpp"
  
Finish::Finish() {
  //setting up the texts
  background.setSize(sf::Vector2f(1024, 768));
  background.setFillColor(sf::Color(20,20,20));
  nametitle = "Enter Name: ";
  name = "";
  
  uifont.loadFromFile("bauerg.ttf");
  
  completed_text.setFont(uifont);
  completed_text.setCharacterSize(50);
  completed_text.setStyle(sf::Text::Bold);
  completed_text.setColor(sf::Color(200,20,40));
  completed_text.setString("Map Completed!");
  completed_text.setPosition(1024/2-completed_text.getLocalBounds().width/2,170);
  
  points_text.setFont(uifont);
  points_text.setCharacterSize(40);
  points_text.setStyle(sf::Text::Bold);
  points_text.setColor(sf::Color(240,240,240));
  
  ranking_text.setFont(uifont);
  ranking_text.setCharacterSize(40);
  ranking_text.setStyle(sf::Text::Bold);
  ranking_text.setColor(sf::Color(240,240,240));
  
  text.setFont(uifont);
  text.setCharacterSize(40);
  text.setStyle(sf::Text::Bold);
  text.setColor(sf::Color(240,240,240));
}

int Finish::run(sf::RenderWindow& window, sf::Event& event)
{
  //updating the texts
  points_ss.str("");
  points_ss << "Points: " << points;
  points_text.setString(points_ss.str());
  points_text.setPosition(1024/2-points_text.getLocalBounds().width/2,275+80);
  
  ranking_ss.str("");
  ranking_ss << "Ranking: " << ranking;
  ranking_text.setString(ranking_ss.str());
  ranking_text.setPosition(1024/2-ranking_text.getLocalBounds().width/2,275+160);
  
  text.setString(nametitle+name);
  text.setPosition(1024/2-text.getLocalBounds().width/2,275+240);
  
  window.draw(*this);
  return processEvents(event);
}

void Finish::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  //draws the objects on the screen
  rt.setView(rt.getDefaultView());
  rt.draw(background);
  rt.draw(points_text);
  rt.draw(ranking_text);
  rt.draw(completed_text);
  rt.draw(text);
}

void Finish::setPoints(size_t p) {points = p;}
void Finish::setRanking(size_t r) {ranking = r;}

int Finish::processEvents(sf::Event& event) {
  //processes keyboard activity
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace) && clock.getElapsedTime().asMilliseconds() > 100) {
    if(name.length() > 0) {
      name.erase(name.end()-1);
      text.setString(nametitle+name);
      clock.restart();
    }
  }
  else if (event.type == sf::Event::TextEntered && clock.getElapsedTime().asMilliseconds() > 100) {
    if (event.text.unicode < 128 && name.length() < 10)
      {
        name += static_cast<char>(event.text.unicode);
        text.setString(nametitle+name);
        clock.restart();
      }
  }
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
    if (event.text.unicode < 128)
      {
        return GAMESTATE_MAINMENU;
      }
  }
  return GAMESTATE_FINISH;
}

std::string Finish::getName() {  
  if(name.length() > 0)
    return name;
  else
    return "Anonymous";
}

