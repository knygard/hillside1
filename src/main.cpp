#include <Box2D/Box2D.h>
#include <map>
#include <sstream>
#include <SFML/Graphics.hpp>
#include "../include/Utilities.hpp"
#include "../include/Game.hpp"
#include "../include/Menu.hpp"
#include "../include/Editor.hpp"
#include "../include/Settings.hpp"
#include "../include/MenuNewGame.hpp"
#include "../include/Finish.hpp"
#include "../include/ResultLoader.hpp"
#include "../include/MenuEdit.hpp"
#include "../include/ScoreManager.hpp"

/* The main function of the game */

int main()
{
  std::map<const int, Screen*> screens;
  int screen = GAMESTATE_MAINMENU;
  int prev = GAMESTATE_MAINMENU;
  sf::Event event;
  windowSettings window_settings;
  sf::ContextSettings settings;
  settings.antialiasingLevel = 8;
  window_settings.width = WINDOW_WIDTH;
  window_settings.height = WINDOW_HEIGHT;

  sf::RenderWindow window(sf::VideoMode(window_settings.width, window_settings.height), 
			  "Hillside Racing", sf::Style::Default, settings);

  window.setVerticalSyncEnabled(true);
  window.setFramerateLimit(60); 
  
  /* Different screens */
      
  Game g;
  screens[GAMESTATE_GAME] = &g;

  ScoreManager h(g);
  screens[GAMESTATE_HIGHSCORES] = &h;
  
  Finish f;
  screens[GAMESTATE_FINISH] = &f;

  MenuNewGame ng(g);
  screens[GAMESTATE_MAPSELECTION] = &ng;

  MapManager highscore_mapselection(g, std::string("maps.txt"));
  screens[GAMESTATE_HIGHSCORES_MAPSELECTION] = &highscore_mapselection;

  Menu mm("HILLSIDE RACING", GAMESTATE_MAINMENU, &g);
  mm.addItem("New game", GAMESTATE_MAPSELECTION);
  mm.addItem("Editor", GAMESTATE_EDITMENU);
  mm.addItem("High scores", GAMESTATE_HIGHSCORES_MAPSELECTION);
  mm.addItem("Exit", -1);
  screens[GAMESTATE_MAINMENU] = &mm;

  Editor e;
  screens[GAMESTATE_EDITOR] = &e;

  MenuEdit em(e);
  screens[GAMESTATE_EDITMENU] = &em;

  /* Helper values */

  bool keyWasPressed = false;

  /* Values to be saved */

  bool save_results = false;
  size_t save_time = 0;
  size_t save_points = 0;
  Player* save_p = NULL;
  
  /* The main loop that loops through the screens */

  while (screen >= 0)
    {
      window.pollEvent(event);

      /* Window closed */

      if (event.type == sf::Event::Closed) 
	      return EXIT_SUCCESS; 
	      
	    /* Saving the results */  
	      
      if (save_results && screen == GAMESTATE_MAINMENU) {
	    	  save_p->saveResult(save_p->getMap()->getMapfilename(), f.getName(), save_points, save_time);
	        save_results = false;
	        save_p = NULL;
	        save_points = 0;
	        save_time = 0;
	    }
	    
	    /* Ending a game */
	      
	    std::vector<Player*> players = g.getPlayers();
	    for (auto p : players) {
	      if (p->getGoal() != 0 && !save_results) {
	      	Result res("", p->getTime(), p->getPoints());
          int game_points = res.calculateGamePoints();
	      	f.setPoints(game_points);
	      	f.setRanking(p->getRanking(p->getMap()->getMapfilename()));
	        prev = GAMESTATE_MAINMENU;
	        screen = GAMESTATE_FINISH;
	        save_results = true;
	        save_p = p;
	        save_time = p->getTime();
	        save_points = p->getPoints();
	        g.resetState();
	      }
      }
      
      /* Updating the high scores */
      
      if (screen == GAMESTATE_HIGHSCORES) {h.update(highscore_mapselection.getMapFile());}
      
      /* Pressing esc leads to the previous screen */
      
      if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape && !keyWasPressed) {
	      if (screen!=GAMESTATE_MAINMENU) {
	        prev = screen;
	        screen = GAMESTATE_MAINMENU;  
	      } else {
	        screen = prev;
	      }
        keyWasPressed = true;
      } else if (event.type == sf::Event::KeyReleased) {
        keyWasPressed = false;
      }
      
      window.clear();
      
      /* Running the current screen */
      
      screen = screens[screen]->run(window, event);
      
      window.display();
      
    }
    return EXIT_SUCCESS;  
}

