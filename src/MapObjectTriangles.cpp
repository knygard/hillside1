#include "../include/MapObjectTriangles.hpp"
#include <iostream>
#include <sstream>

MapObjectTriangles::MapObjectTriangles(b2World* world, std::string texname, std::vector<sf::Vector3<sf::Vector2f>>& triangles, bool impassable, int type) : MapObject(type), texname(texname), t(NULL), impassable(impassable) {
  b2BodyDef bd;
  b2FixtureDef fd;
  b2EdgeShape e1,e2,e3;
  if (world!=NULL&&impassable) {
    shapeBody = world->CreateBody(&bd);
    shapeBody->SetUserData(this);
    fd.density = 0.0f;
    fd.friction = 1.0f;
  }
  
  for (sf::Vector3<sf::Vector2f> tri : triangles) {
    this->triangles.push_back(tri);
    if (world!=NULL&&impassable) {
      e1.Set(b2Vec2(tri.x.x,tri.x.y),b2Vec2(tri.y.x,tri.y.y));
      e2.Set(b2Vec2(tri.y.x,tri.y.y),b2Vec2(tri.z.x,tri.z.y));
      e3.Set(b2Vec2(tri.z.x,tri.z.y),b2Vec2(tri.x.x,tri.x.y));
      fd.shape = &e1;
      shapeBody->CreateFixture(&fd);
      fd.shape = &e2;
      shapeBody->CreateFixture(&fd);
      fd.shape = &e3;
      shapeBody->CreateFixture(&fd);
    }
  }
}

void MapObjectTriangles::updateTextures(std::map<std::string, sf::Texture*>& textures) {
  t = textures[texname];
}

MapObjectTriangles::MapObjectTriangles(b2World* world, std::string texname, bool impassable, std::istringstream& in_line, int type) : MapObject(type), texname(texname), t(NULL), impassable(impassable) {
  b2BodyDef bd;
  b2FixtureDef fd;
  b2EdgeShape e1,e2,e3;
  int i;
  type = MAPTYPE_FG;
  if (world!=NULL&&impassable) {
    shapeBody = world->CreateBody(&bd);
    shapeBody->SetUserData(this);
    fd.density = 0.0f;
    fd.friction = 0.6f;
  }
  in_line >> i;
  for (int j=0;j<i;j++) {
    float x1,y1,x2,y2,x3,y3;
    in_line >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
    this->triangles.push_back(sf::Vector3<sf::Vector2f>(sf::Vector2f(x1,y1),
							sf::Vector2f(x2,y2),
							sf::Vector2f(x3,y3)));
  }
  for (sf::Vector3<sf::Vector2f> tri : triangles) {
    if (world!=NULL&&impassable) {
      e1.Set(b2Vec2(tri.x.x,tri.x.y),b2Vec2(tri.y.x,tri.y.y));
      e2.Set(b2Vec2(tri.y.x,tri.y.y),b2Vec2(tri.z.x,tri.z.y));
      e3.Set(b2Vec2(tri.z.x,tri.z.y),b2Vec2(tri.x.x,tri.x.y));
      fd.shape = &e1;
      shapeBody->CreateFixture(&fd);
      fd.shape = &e2;
      shapeBody->CreateFixture(&fd);
      fd.shape = &e3;
      shapeBody->CreateFixture(&fd);
    }
  }
}

std::string MapObjectTriangles::getAsText() const {
  std::stringstream s;
  std::string texname;
  if (impassable)
    texname = "light";
  else
    texname = "dark";
  s << "triangles " 
    << texname << " " 
    << type << " " 
    << (impassable ? "0" : "1") << " " 
    << triangles.size() << " ";
  for (sf::Vector3<sf::Vector2f> tri : triangles) {
    s << tri.x.x << " "
      << tri.x.y << " "
      << tri.y.x << " "
      << tri.y.y << " "
      << tri.z.x << " "
      << tri.z.y << " ";
  }
  s << std::endl;
  return s.str();
}
  
void MapObjectTriangles::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  static GLint s_vector[4] = { 8, 0, 0, 0 };
  static GLint t_vector[4] = { 0, 8, 0, 0 };

  rt.draw(shape);
  glFlush();
  rt.pushGLStates();
  glMatrixMode (GL_MODELVIEW);
  
  if (t!=NULL) {
    /* Lets enable textures and generate automatically the texture coordinates */
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);        
    sf::Texture::bind(t);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);    
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGeniv(GL_S, GL_OBJECT_PLANE, s_vector);
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGeniv(GL_T, GL_OBJECT_PLANE, t_vector);
    
    /* Draw the ground triangles*/
    glBegin(GL_TRIANGLES);
    for (sf::Vector3<sf::Vector2f> tri : triangles) {
      glVertex3f((float)(tri.x.x*20),(float)(-tri.x.y*20), 1.0);
      glVertex3f((float)(tri.y.x*20),(float)(-tri.y.y*20), 1.0);
      glVertex3f((float)(tri.z.x*20),(float)(-tri.z.y*20), 1.0);
    }
    glEnd();
    
    /* Unbind texture */
    sf::Texture::bind(NULL);
    glDisable(GL_TEXTURE_2D);
  } else {
    /* Draw wireframe version of ground in case the texture was missing for some reason */
    glColor3f (1.0, 0.0, 0.0);
    glLineWidth(2);
    glBegin(GL_LINES);
    for (sf::Vector3<sf::Vector2f> tri : triangles) {
      glVertex3f((float)(tri.x.x*20),(float)(-tri.x.y*20), 1.0);
      glVertex3f((float)(tri.y.x*20),(float)(-tri.y.y*20), 1.0);
      glVertex3f((float)(tri.y.x*20),(float)(-tri.y.y*20), 1.0);
      glVertex3f((float)(tri.z.x*20),(float)(-tri.z.y*20), 1.0);
      glVertex3f((float)(tri.z.x*20),(float)(-tri.z.y*20), 1.0);
      glVertex3f((float)(tri.x.x*20),(float)(-tri.x.y*20), 1.0);
    }
    glEnd();
  }
  glFlush();
  rt.popGLStates();
}
