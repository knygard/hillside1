#include "../include/MapObjectGoal.hpp"

MapObjectGoal::MapObjectGoal(b2World* world, std::istringstream& s) : MapObjectCollectable(world, s, MAPOBJ_IDENT_GOAL) 
{
  init();
}

MapObjectGoal::MapObjectGoal(b2World* world, float x, float y) : MapObjectCollectable(world, x,y, MAPOBJ_IDENT_GOAL) 
{
  init();
}

void MapObjectGoal::init() {
  b2Vec2 vertices[4];
  vertices[0].Set(-1,  1);
  vertices[1].Set(-1,  -1);
  vertices[2].Set( 1.5, -1);
  vertices[3].Set( 1.5,  1);
  
  polyShape.Set(vertices, 4);
  goal = B2toSFRenderer::PolygonToSFConvex(polyShape);
  goaltexture.loadFromFile("goal.png");
  goaltexture.setSmooth(true);
  goal.setTexture(&goaltexture, true);
  goal.setOrigin(1, 1);
  goal.setPosition(x*20*2,-y*20*2);
  
  addToWorld();
}

std::string MapObjectGoal::getName() const { return "goal"; }

void MapObjectGoal::draw(sf::RenderTarget &rt, sf::RenderStates states) const
{
  if(!collected) {rt.draw(goal);}
}

b2Shape& MapObjectGoal::getShape() {
  return polyShape;
}

