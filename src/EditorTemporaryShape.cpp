#include "../include/EditorTemporaryShape.hpp"

void EditorTemporaryShape::update(float x, float y) {
  curx=x;
  cury=y;
  
  snap_on = false;
  sx = 0;
  sy = 0;
  for (sf::Vector3<sf::Vector2f> p : points) {
    if ((curx>p.x.x-1)&&(curx<p.x.x+1)&&(cury<p.x.y+1)&&(cury>p.x.y-1)) {
      sx = p.x.x;
      sy = p.x.y;
      snap_on = true;
    } else if ((curx>p.y.x-1)&&(curx<p.y.x+1)&&(cury<p.y.y+1)&&(cury>p.y.y-1)) {
      sx = p.y.x;
      sy = p.y.y;
      snap_on = true;
    } else if ((curx>p.z.x-1)&&(curx<p.z.x+1)&&(cury<p.z.y+1)&&(cury>p.z.y-1)) {
      sx = p.z.x;
      sy = p.z.y;
      snap_on = true;
    }
  }
}

std::vector<sf::Vector3<sf::Vector2f>>& EditorTemporaryShape::getTriangles() {
  return points;
}

void EditorTemporaryShape::clear(int type) {
  this->type=type;
  phase = 0;
  snap_on = false;
  points.clear();
}

void EditorTemporaryShape::clear() {
  phase = 0;
  snap_on = false;
  points.clear();
}

void EditorTemporaryShape::addPoint(float x, float y) {
  if (type==0)
    return;
  if (snap_on) {
    x = sx;
    y = sy;
  }
  
  if (phase==0) {
    p1 = sf::Vector2f(x,y);
    phase = 1;
  } else if (phase==1) {
    p2 = sf::Vector2f(x,y);
    phase = 2;
  } else if (phase==2) {
    points.push_back(sf::Vector3<sf::Vector2f>(p1,p2,sf::Vector2f(x,y)));
    if (type==ED_TRIANGLE) {
      // In case of normal triangles start from scratch when finished
      phase=0;
    } else if (type==ED_TRIANGLESTRIP) { 
      // In case of trianglestrip the previous point and this point will be two first points
      p1 = p2;
      p2 = sf::Vector2f(x,y);
    } else if (type==ED_TRIANGLEFAN) {
      // In case of fan the first point will be point #0 and second is this point.
      // If we don't touch p1 it will remain the first point.
      p2 = sf::Vector2f(x,y);
    }
  }
}

void EditorTemporaryShape::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  if (type==0)
    return;
  glFlush();
  rt.pushGLStates();
  //rt.resetGLStates();
  glMatrixMode (GL_MODELVIEW);
  
  glColor4f (1.0, 0.0, 0.0, 1.0f);
  glLineWidth(3);
  glBegin(GL_LINES);
  for (sf::Vector3<sf::Vector2f> v : points) { // Draw crosses
    EdDrawX(v.x);
    EdDrawX(v.y);
    EdDrawX(v.z);
  }
  for (sf::Vector3<sf::Vector2f> v : points) { // Draw sides
    glVertex3f((float)(v.x.x*20),(float)(-v.x.y*20), 1.0);
    glVertex3f((float)(v.y.x*20),(float)(-v.y.y*20), 1.0);
    glVertex3f((float)(v.y.x*20),(float)(-v.y.y*20), 1.0);
    glVertex3f((float)(v.z.x*20),(float)(-v.z.y*20), 1.0);
    glVertex3f((float)(v.z.x*20),(float)(-v.z.y*20), 1.0);
    glVertex3f((float)(v.x.x*20),(float)(-v.x.y*20), 1.0);
  }
  
  if (phase==1) { // If one point is drawn then draw line to that point from cursor
    glVertex3f((float)(p1.x*20),(float)(-p1.y*20), 1.0);
    glVertex3f((float)(curx*20),(float)(-cury*20), 1.0);
    
    glVertex3f((float)(p1.x*20-5),(float)(-p1.y*20-5), 1.0);
    glVertex3f((float)(p1.x*20+5),(float)(-p1.y*20+5), 1.0);
    glVertex3f((float)(p1.x*20-5),(float)(-p1.y*20+5), 1.0);
    glVertex3f((float)(p1.x*20+5),(float)(-p1.y*20-5), 1.0);
    
  } else if (phase==2) { // If two points were draw draw triangle where 3rd point is cursor
    glVertex3f((float)(p1.x*20),(float)(-p1.y*20), 1.0);
    glVertex3f((float)(p2.x*20),(float)(-p2.y*20), 1.0);
    glVertex3f((float)(p1.x*20),(float)(-p1.y*20), 1.0);
    glVertex3f((float)(curx*20),(float)(-cury*20), 1.0);
    glVertex3f((float)(p2.x*20),(float)(-p2.y*20), 1.0);
    glVertex3f((float)(curx*20),(float)(-cury*20), 1.0);
    
    EdDrawX(p1);
    EdDrawX(p2);
  }
  
  // Draw X at cursor
  EdDrawX(sf::Vector2f(curx,cury));
  
  glEnd();
  glColor4f (1.0, 0.0, 0.0, 0.2f);
  glLineWidth(2);
  glBegin(GL_TRIANGLES);
  for (sf::Vector3<sf::Vector2f> v : points) { // Draw filled triangles
    glVertex3f((float)(v.x.x*20),(float)(-v.x.y*20), 1.0);
    glVertex3f((float)(v.y.x*20),(float)(-v.y.y*20), 1.0);
    glVertex3f((float)(v.z.x*20),(float)(-v.z.y*20), 1.0);
  }
  if (phase==2) { // If two points were draw draw triangle where 3rd point is cursor
    glVertex3f((float)(p1.x*20),(float)(-p1.y*20), 1.0);
    glVertex3f((float)(p2.x*20),(float)(-p2.y*20), 1.0);
    glVertex3f((float)(curx*20),(float)(-cury*20), 1.0);
  }
  glEnd();
  
  if (snap_on) { // If old vertex detected nearby draw blue marker to indicate snap point
    glColor4f (0.0, 0.0, 1.0, 1.0f);
    glLineWidth(3);
    glBegin(GL_LINES);
    EdDrawX(sf::Vector2f(sx,sy));
    glEnd();
  }
  
  glFlush();
  //rt.resetGLStates();
  rt.popGLStates();
  glFlush();
}
