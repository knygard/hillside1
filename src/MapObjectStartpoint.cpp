#include "../include/MapObjectStartpoint.hpp"

MapObjectStartpoint::MapObjectStartpoint(std::istringstream& s) : MapObject(MAPTYPE_EDITONLY) {
  s >> x >> y;
  init();
}

MapObjectStartpoint::MapObjectStartpoint(float x, float y) : MapObject(MAPTYPE_EDITONLY), x(x), y(y) {
  init();
}

void MapObjectStartpoint::init() {
  texture.loadFromFile("startpoint.png");
  texture.setSmooth(true);
  shape.setRadius(20);
  shape.setTexture(&texture, true);
  shape.setOrigin(shape.getRadius(), shape.getRadius());
  shape.setPosition(x*20*2,-y*20*2);
}

std::string MapObjectStartpoint::getAsText() const {
  std::stringstream s;
  s << "startpoint "
    << x << " "
    << y << std::endl;
  return s.str();
}

void MapObjectStartpoint::draw(sf::RenderTarget &rt, sf::RenderStates states) const
{
  rt.draw(shape);
}
