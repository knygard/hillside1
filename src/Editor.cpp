#include "../include/Editor.hpp"

Editor::Editor() : mousex(0), mousey(0), fb(10,WINDOW_HEIGHT-ED_BS) {
  // Resets the views
  skyView.reset(   sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
  staticView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
  gameView.reset(  sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
  frontView.reset( sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
  cursorView.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
  map = new Map();
  posx=0;
  posy=0;
}

void Editor::load(std::string filename, int theme) {
  // If there is already map remove it first
  if (map!=NULL)
    delete map;
  // If filename is NEWMAP then create empty map instead of loading one
  if (filename=="NEWMAP") {
    map = new Map();
    addTheme(map, theme);
  } else {
    map = new Map(filename);
  }
  // Let all objects to set their textures
  for (EditorItem* i : eitems.getItems()) {
    map->loadTexture(i->name, i->filename);
  }
}

void Editor::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  
  // Quite dirty trick to avoid sfml bug which causes the sfml to ignore the view change
  // when using opengl. At least one sfml shape has to be drawn before opengl graphics to
  // really change the view
  sf::ConvexShape sh;
  sh.setPosition(0,0);
  sh.setPointCount(3);
  sh.setPoint(0,sf::Vector2f(0,0));
  sh.setPoint(1,sf::Vector2f(1,0));
  sh.setPoint(2,sf::Vector2f(0,1));
  
  target.setView(skyView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_SKY);    // Draw sky behind the playfield
  
  target.setView(gameView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_BG);     // Draws everything behind player
  map->draw(target, states, MAPTYPE_FG|MAPTYPE_EDITONLY); // Draws everything in front of player
  target.draw(s);
  
  target.setView(frontView);
  target.draw(sh);
  map->draw(target, states, MAPTYPE_FRONT);  // Draw Decorations in front of the playfield
  
  
  target.setView(cursorView);
  target.draw(sh);
  if (newObj!=NULL)
    target.draw(*newObj);                    // If some item is selected it is in newObj so draw it under cursor
  
  target.setView(staticView);
  target.draw(menu);                         // Draw the tool menu
  target.draw(eitems);                       // Draw the item list
  target.draw(fb);                           // Draw the front/back button
}

int Editor::run(sf::RenderWindow& window, sf::Event& event) {
  window.draw(*this);

  if (saveNewMap) {
    handleSaveNewMap(window, event);
    return GAMESTATE_EDITOR;
  }

  handleInput();
 
  mousex = sf::Mouse().getPosition(window).x;
  mousey = sf::Mouse().getPosition(window).y;
 
  // Update the cursor position to currently drawn shape
  s.update((mousex+posx-1024/2)/20,(-mousey-posy+768/2)/20);
  
  if (sf::Mouse().isButtonPressed(sf::Mouse::Left)) {
    // Place item/shape point marked by tool/itemlist
    if (!mclick_state) {
      mclick_state=true;
      if (mousey>ED_BS&&mousey<768-ED_BS) {
	if (toolSelection!=-1)
	  handleToolPlacement();
	if (itemSelection!=-1)
	  handleItemPlacement();
      } 
    }	
  } else if (sf::Mouse().isButtonPressed(sf::Mouse::Right)) {
    // Convert shape which is under construction to map item
    if (!mclick_state) {
      mclick_state=true;
      TemporaryShapeToMap();
    }
  } else {
    mclick_state=false;
  }

  // If item selection is changed (and item is selected) change the object shown on cursor
  if (itemSelection!=eitems.getId()&&eitems.getId()!=-1) {
    toolSelection = -1;
    menu.setSelection(-1);
    itemSelection = eitems.getId();
    if (newObj!=NULL)
      delete newObj;    
    newObj = new MapObjectItem(eitems.getItem(eitems.getId())->name,
			       (layer ? MAPTYPE_BG : MAPTYPE_FG), 0,0,
			       eitems.getItem(eitems.getId())->size.x,
			       eitems.getItem(eitems.getId())->size.y);
    newObj->updateTextures(map->getTextures());
    menu.setDesc(eitems.getItem(eitems.getId())->name);
  }
  
  // If tool selection is changed (and item type of tool is selected) change the object shown under cursor  
  // Or start drawing the shape
  if (toolSelection!=menu.getId()&&menu.getId()!=-1) {
    eitems.setSelection(-1);
    itemSelection = -1;
    toolSelection = menu.getId();
    if (newObj!=NULL)
      delete newObj;
    if (toolSelection!=-1) {
      switch (toolSelection) {
      case EDBTN_BOOST:
	newObj = new MapObjectBoost(NULL, 0,0);
	s.clear(0);
	break;
      case EDBTN_COIN:
	newObj = new MapObjectCoin(NULL, 0,0);
	s.clear(0);
	break;
      case EDBTN_GOAL:
	newObj = new MapObjectGoal(NULL, 0,0); 
	s.clear(0);
	break;
      case EDBTN_ITEM:
	newObj = new MapObjectItem("bridge",MAPTYPE_FG, 0,0, 550,200);
	s.clear(0);
	break;
      case EDBTN_PL1:
	newObj = new MapObjectStartpoint(0,0);
	s.clear(0);
	break;
      case EDBTN_RANDOM:
	newObj = new MapObjectRandom(NULL, 0,0);
	s.clear(0);
	break;
      case EDBTN_SAVE:
	newObj = NULL;
	s.clear(0);
	// Save changes if file existed
	if (map->getMapfilename()!="") {
	  std::ofstream o(map->getMapfilename());
	  o << *map;
	  menu.setDesc("Changes saved!");
	} else {
	  saveNewMapState = 0;
	  saveNewMap = true;
	}
	break;
      case EDBTN_TRIANGLES:
	newObj = NULL;
	s.clear(ED_TRIANGLE);
	break;
      case EDBTN_TRIANGLEFAN:
	newObj = NULL;
	s.clear(ED_TRIANGLEFAN);
	break;
      case EDBTN_TRIANGLESTRIP:
	newObj = NULL;
	s.clear(ED_TRIANGLESTRIP);
	break;
      case EDBTN_LOAD:
      case EDBTN_PL2:
      case EDBTN_DELETE:
      case EDBTN_EDIT:
	newObj = NULL;
	s.clear(0);
	break;
      }
    }
  }
  
  // Let the menus update their contents and selection based on mouse input
  menu.update(mousex, mousey, sf::Mouse().isButtonPressed(sf::Mouse::Left));
  eitems.update(mousex, mousey, sf::Mouse().isButtonPressed(sf::Mouse::Left));
  // Update the positions of different parts of game field
  gameView.setCenter(posx, posy);
  skyView.setCenter(posx*0.2, posy*0.2);
  frontView.setCenter(posx*1.05, posy*1.05);
  cursorView.setCenter(-mousex+(1024/2), -mousey+(768/2));
  
  map->update();

  return GAMESTATE_EDITOR;
}

void Editor::handleInput() {
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))   posy-=50;
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) posy+=50;
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) posx-=50;
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))posx+=50;
  
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
    layer=0;
    fb.setState(layer);
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
    layer=1;
    fb.setState(layer);
  }
}

void Editor::handleToolPlacement() {
  switch(toolSelection) {
  case EDBTN_TRIANGLES:
  case EDBTN_TRIANGLESTRIP:
  case EDBTN_TRIANGLEFAN:
    s.addPoint((mousex+posx-1024/2)/20,(-mousey-posy+768/2)/20);
    break;
  case EDBTN_COIN:
    map->addObject(new MapObjectCoin(NULL, (mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40));
    break;
  case EDBTN_BOOST:
    map->addObject(new MapObjectBoost(NULL, (mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40));
    break;
  case EDBTN_GOAL:
    map->addObject(new MapObjectGoal(NULL, (mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40));
    break;
  case EDBTN_RANDOM:
    map->addObject(new MapObjectRandom(NULL, (mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40));
    break;
  case EDBTN_PL1:
    map->addObject(new MapObjectStartpoint((mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40));
    break;
  }
}

void Editor::handleItemPlacement() {
  MapObject* nm;
  nm = new MapObjectItem(eitems.getItem(eitems.getId())->name, 
			 (layer ? MAPTYPE_FG : MAPTYPE_BG), 
			 (mousex+posx-1024/2)/40,(-mousey-posy+768/2)/40,
			 eitems.getItem(eitems.getId())->size.x,
			 eitems.getItem(eitems.getId())->size.y);
  nm->updateTextures(map->getTextures());
  map->addObject(nm);
}

void Editor::TemporaryShapeToMap() {
  MapObject* nm;
  if (layer==1)
    nm = new MapObjectTriangles(NULL, "light", s.getTriangles(), true, MAPTYPE_FG);
  else
    nm = new MapObjectTriangles(NULL, "dark", s.getTriangles(), false, MAPTYPE_BG);
  nm->updateTextures(map->getTextures());
  map->addObject(nm);
  s.clear();
}

void Editor::handleSaveNewMap(sf::RenderWindow& window, sf::Event& event) {
  sf::Text caption;
  sf::Font font;
  std::stringstream strs;
  unsigned char newchar=0;

  // Handle the text input
  if (event.type == sf::Event::TextEntered && !saveNewKeyPressed) {
    saveNewKeyPressed=true;
    if (event.text.unicode < 128)
      newchar = static_cast<unsigned char>(event.text.unicode);
  }

  if (event.type != sf::Event::TextEntered&&event.type == sf::Event::KeyReleased)
    saveNewKeyPressed=false;

  font.loadFromFile("sansation.ttf");
  if (saveNewMapState==0) {
    if ((newchar>=48&&newchar<=57)||
	(newchar>=65&&newchar<=90)|| 
	(newchar>=97&&newchar<=172)||
	newchar==32)
      saveNewMapName += newchar;
    strs << "Enter new map name:" << saveNewMapName;
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return))&&!saveNewKeyPressed) {
      saveNewKeyPressed=true;
      saveNewMapState=1;
    }
  } else if (saveNewMapState==1) {
    strs << "Enter filename for new map:" << saveNewMapFname;
    if ((newchar>=48&&newchar<=57)||
	(newchar>=65&&newchar<=90)|| 
	(newchar>=97&&newchar<=172))
      saveNewMapFname += newchar;
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return))&&!saveNewKeyPressed) {
      saveNewKeyPressed=true;
      saveNewMapState=2;
    }
  } else {
    map->saveMap(saveNewMapFname);
    std::ofstream o("maps.txt", std::ios_base::app);
    o << saveNewMapFname << std::endl;
    o << saveNewMapName << std::endl;
    // Do the saving
    saveNewMapName="";
    saveNewMapFname="";
    saveNewMap=false;
    return;
  }

  caption.setFont(font);
  caption.setCharacterSize(25);
  caption.setColor(sf::Color::Blue);
  caption.setString(strs.str());
  caption.setPosition(WINDOW_WIDTH/2-caption.getLocalBounds().width/2,
		      WINDOW_HEIGHT/2-caption.getLocalBounds().height/2);

  window.draw(caption);
}

void Editor::addTheme(Map* map, int theme) {
  if (theme==0) {
    map->loadTexture("bg","db_bg.png");
    map->loadTexture("fg","db_fg.png");
    map->loadTexture("dark","db_dark.png");
    map->loadTexture("light","db_light.png");
    map->addObject(new MapObjectBackground(map->loadTexture("bg"), 
					   sf::Vector2<float>(100,300), sf::Vector2<float>(2,1.5)));
    map->addObject(new MapObjectBackground(map->loadTexture("fg"), 
					   sf::Vector2<float>(100,-1000), sf::Vector2<float>(20,2),MAPTYPE_FRONT));
  } 
  if (theme==1) {
    map->loadTexture("bg","map_canyon_bg.png");
    map->loadTexture("fg","map_canyon_fg.png");
    map->loadTexture("dark","ground2.png");
    map->loadTexture("light","ground3.png");
    map->addObject(new MapObjectBackground(map->loadTexture("bg"), 
					   sf::Vector2<float>(100,300), sf::Vector2<float>(2,1.5)));
    map->addObject(new MapObjectBackground(map->loadTexture("fg"), 
					   sf::Vector2<float>(100,-1000), sf::Vector2<float>(10,2),MAPTYPE_FRONT));
  }

  if (theme==2) {
    map->loadTexture("bg","terrain_England_background.png");
    map->loadTexture("fg","terrain_England_foreground.png");
    map->loadTexture("dark","ground2b.png");
    map->loadTexture("light","ground1.png");
    map->addObject(new MapObjectBackground(map->loadTexture("bg"), 
					   sf::Vector2<float>(100,300), sf::Vector2<float>(2,1.5)));
    map->addObject(new MapObjectBackground(map->loadTexture("fg"), 
					   sf::Vector2<float>(100,-1000), sf::Vector2<float>(10,2),MAPTYPE_FRONT));
  }
  map->loadTexture("bridge","bridge.png");
}
