#include "../include/HyperCab.hpp"

HyperCab::HyperCab(b2World* world) : TwoWheelVehicle(world)
{
  /* Parameters */
  {
    maxSpeed = 55.0f;
    maxMotorSpeed = 40.0f;
    angularImpulse = 35.0f;
    boostFactor = 2.0f;
    acceleration = 30.0f;
    driveType = FourWheelDrive;
  }

  /* Chassis */
  {
    chassisTexturePath = "HyperCab_body.png";

    chassis_distance = 2.3f;
    chassisVertices[0].Set(-2.6f, -0.3f);
    chassisVertices[1].Set(2.6f, -0.3f);
    chassisVertices[2].Set(2.6f, 0.4f);
    chassisVertices[3].Set(1.5f, 1.0f);
    chassisVertices[4].Set(-1.5f, 1.0f);
    chassisVertices[5].Set(-2.6f, 0.4f);

    // Material
    chassisFixture.density = 0.6f;
    chassisFixture.friction = 0.3f;
    chassisFixture.restitution = 0.05;
  }

  /* Wheels */
  {
    wheelTexturePath = "tire.png";
    rearWheelRadius = 0.7f;
    frontWheelRadius = 0.7f;

    rearWheel_position.Set(-1.4f, 1.4f);
    frontWheel_position.Set(1.6f, 1.4f);

    // Motors (joints)

    rearWheelJoint.maxMotorTorque = 55.0f;
    rearWheelJoint.frequencyHz = 3;
    rearWheelJoint.dampingRatio = 0.2;

    frontWheelJoint.maxMotorTorque = 145.0f;
    frontWheelJoint.frequencyHz = 3;
    frontWheelJoint.dampingRatio = 0.2;

    // Materials

    rearWheelFixture.density = 0.8f;
    rearWheelFixture.friction = 1.3f;
    rearWheelFixture.restitution = 0.15f;

    frontWheelFixture.density = 0.8f;
    frontWheelFixture.friction = 1.3f;
    frontWheelFixture.restitution = 0.15f;
  }

  // Initialization !important
  initialize();
}