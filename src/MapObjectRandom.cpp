#include "../include/MapObjectRandom.hpp"

MapObjectRandom::MapObjectRandom(b2World* world, std::istringstream& s) : MapObjectCollectable(world, s, MAPOBJ_IDENT_RANDOM) {
  init();
  type = MAPTYPE_EDITONLY;
}

MapObjectRandom::MapObjectRandom(b2World* world, float x, float y) : MapObjectCollectable(world, x,y, MAPOBJ_IDENT_RANDOM) {
  init();
  type = MAPTYPE_EDITONLY;
}

void MapObjectRandom::init() {
  myShape.m_radius = 1.0f;
  random = B2toSFRenderer::CircleToSFCircle(myShape);
  randomtexture.loadFromFile("random.png");
  randomtexture.setSmooth(true);
  random.setTexture(&randomtexture, true);
  random.setOrigin(random.getRadius(), random.getRadius());
  random.setPosition(x*20*2,-y*20*2);
  addToWorld();
}

std::string MapObjectRandom::getAsText() const { 
  std::stringstream s; 
  s << "random " 
    << x << " "
    << y << std::endl;
  return s.str();
}

void MapObjectRandom::draw(sf::RenderTarget &rt, sf::RenderStates states) const
{
  if(!collected) {rt.draw(random);}
}

std::string MapObjectRandom::getName() const { return "random"; }
b2Shape& MapObjectRandom::getShape() { return myShape; }
