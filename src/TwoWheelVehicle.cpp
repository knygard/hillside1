#include "../include/TwoWheelVehicle.hpp"

TwoWheelVehicle::TwoWheelVehicle(b2World* world) : MapObjectVehicle(world)
{ }

void TwoWheelVehicle::initialize() {
  b2BodyDef bd;
  bd.type = b2_dynamicBody;


  // CHASSIS (b2Chassis)
  {
    // Shape
    b2PolygonShape polygonShape;
    polygonShape.Set(chassisVertices, 6);
    chassisFixture.shape = &polygonShape;
    sfChassis = B2toSFRenderer::PolygonToSFConvex(polygonShape);

    // b2Body
    bd.position.Set(0.0f, chassis_distance);
    b2Chassis = world->CreateBody(&bd);
    b2Chassis->CreateFixture(&chassisFixture);
    b2Chassis->SetUserData(this); // This is the data which will be passed to handler in contact to another object

    // Set chassis texture
    chassisTexture.loadFromFile(chassisTexturePath);
    chassisTexture.setSmooth(true);
    sfChassis.setTexture(&chassisTexture, true);
  }

  // WHEELS
  {
    wheelTexture.loadFromFile(wheelTexturePath);
    wheelTexture.setSmooth(true);

    b2CircleShape rearWheelCircle;
    rearWheelCircle.m_radius = rearWheelRadius;
    rearWheelFixture.shape = &rearWheelCircle;
    sfRearWheel = B2toSFRenderer::CircleToSFCircle(rearWheelCircle);
    sfRearWheel.setTexture(&wheelTexture, true);

    bd.position.Set(rearWheel_position.x, rearWheel_position.y);
    b2RearWheel = world->CreateBody(&bd);
    b2RearWheel->CreateFixture(&rearWheelFixture);

    b2CircleShape frontWheelCircle;
    frontWheelCircle.m_radius = frontWheelRadius;
    frontWheelFixture.shape = &frontWheelCircle;
    sfFrontWheel = B2toSFRenderer::CircleToSFCircle(frontWheelCircle);
    sfFrontWheel.setTexture(&wheelTexture, true);

    bd.position.Set(frontWheel_position.x, frontWheel_position.y);
    b2FrontWheel = world->CreateBody(&bd);
    b2FrontWheel->CreateFixture(&frontWheelFixture);
  }

  // WHEEL JOINTS
  {
    b2Vec2 axis(0.0f, 1.0f);

    rearWheelJoint.Initialize(b2Chassis, b2RearWheel, b2RearWheel->GetPosition(), axis);
    b2RearSpring = (b2WheelJoint*)world->CreateJoint(&rearWheelJoint);

    frontWheelJoint.Initialize(b2Chassis, b2FrontWheel, b2FrontWheel->GetPosition(), axis);
    b2FrontSpring = (b2WheelJoint*)world->CreateJoint(&frontWheelJoint);
  }
}

void TwoWheelVehicle::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(sfRearWheel);
  rt.draw(sfFrontWheel);
  rt.draw(sfChassis);
}

void TwoWheelVehicle::setPosition(float x, float y) {
  b2RearWheel->SetTransform(b2Vec2(x-2,y-0.6), 0);  // Approximation for placement is enough
  b2FrontWheel->SetTransform(b2Vec2(x+2,y-0.6), 0);
  b2Chassis->SetTransform(b2Vec2(x,y), 0);
}

void TwoWheelVehicle::update() {

  limitSpeed();

  sfChassis.setPosition(b2Chassis->GetPosition().x*PIX_PER_M,
                        b2Chassis->GetPosition().y*PIX_PER_M*(-1));
  sfChassis.setRotation(b2Chassis->GetAngle() * (-180.0f / b2_pi));

  sfRearWheel.setOrigin(sfRearWheel.getRadius(), sfRearWheel.getRadius());
  sfRearWheel.setPosition((b2RearWheel->GetPosition().x )*PIX_PER_M,
                          (b2RearWheel->GetPosition().y)*PIX_PER_M*(-1));
  sfRearWheel.setRotation(b2RearWheel->GetAngle() * (-180.0f / b2_pi));

  sfFrontWheel.setOrigin(sfFrontWheel.getRadius(), sfFrontWheel.getRadius());
  sfFrontWheel.setPosition((b2FrontWheel->GetPosition().x)*PIX_PER_M,
                           (b2FrontWheel->GetPosition().y)*PIX_PER_M*(-1));
  sfFrontWheel.setRotation(b2FrontWheel->GetAngle() * (-180.0f / b2_pi));
}

void TwoWheelVehicle::setSpeed(float speed) {
  b2RearSpring->SetMotorSpeed(-speed);
  b2FrontSpring->SetMotorSpeed(-speed);
}

void TwoWheelVehicle::disableMotor() {
  b2RearSpring->EnableMotor(false);
  b2FrontSpring->EnableMotor(false);
}

void TwoWheelVehicle::enableMotor() {
  disableMotor();
  if (driveType == FourWheelDrive || driveType == RearWheelDrive)
    b2RearSpring->EnableMotor(true);
  if (driveType == FourWheelDrive || driveType == FrontWheelDrive)
    b2FrontSpring->EnableMotor(true);
}

void TwoWheelVehicle::forward(float deltaTime) {
  if (b2RearWheel->GetLinearVelocity().x < -5.0f)
    stop();
  else
    accelerate(deltaTime*100*acceleration);
}

void TwoWheelVehicle::backward(float deltaTime) {
  if (b2FrontWheel->GetLinearVelocity().x > 5.0f)
    stop();
  else
    accelerate(deltaTime*100*(-acceleration));
}

void TwoWheelVehicle::stop() {
  b2RearSpring->EnableMotor(true);
  b2RearSpring->SetMotorSpeed(0.0f);
  b2FrontSpring->EnableMotor(true);
  b2FrontSpring->SetMotorSpeed(0.0f);
}

void TwoWheelVehicle::accelerate(float32 change) {
  enableMotor();
  setSpeed(getMotorSpeed()+change);
}

void TwoWheelVehicle::idle() {
  b2RearSpring->SetMotorSpeed(0.0f);
  b2FrontSpring->SetMotorSpeed(0.0f);
  disableMotor();
}

void TwoWheelVehicle::tilt(bool clockwise) {

  if (abs(b2Chassis->GetAngularVelocity()) > 2)
    return;

  if (clockwise)
    b2Chassis->ApplyAngularImpulse(-angularImpulse);
  else
    b2Chassis->ApplyAngularImpulse(angularImpulse);
}

void TwoWheelVehicle::boost() {

  if (!getCollectables(MAPOBJ_IDENT_BOOST).size())
    return;
  
  std::vector<b2Body*> bodies;
  bodies.push_back(b2RearWheel);
  bodies.push_back(b2FrontWheel);
  bodies.push_back(b2Chassis);

  for (b2Body* body : bodies) {
    auto vel = body->GetLinearVelocity();
    vel.x = vel.x * boostFactor;
    if (vel.x > 2.0*maxSpeed)
      vel.x = 2.0*maxSpeed;
    vel.y = vel.y * 1.3;
    if (vel.y > 0.5*maxSpeed)
      vel.y = 0.5*maxSpeed;
    body->SetLinearVelocity(vel);
  }
  
  deleteCollected(MAPOBJ_IDENT_BOOST);
}

void TwoWheelVehicle::limitSpeed() {
  if (getMotorSpeed() > maxMotorSpeed)
    setSpeed(maxMotorSpeed);
  else if (getMotorSpeed() < -maxMotorSpeed)
    setSpeed(0.65*-maxMotorSpeed); // go slower backward
}
