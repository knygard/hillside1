#include "../include/Menu.hpp"

MenuChoice::MenuChoice(std::string text, int state, int x, int y, sf::Font& font, bool changemenu, MenuChoiceCallBack* call, int id) : ypos(y), nextState(state), changemenu(changemenu), callme(call), id(id) {
  name.setFont(font);
  name.setStyle(sf::Text::Bold);
  name.setColor(sf::Color::White);
  name.setCharacterSize(40);
  name.setString(text);
  name.setPosition(1024/2-name.getLocalBounds().width/2,ypos);
}

void MenuChoice::setSelected(bool sel) {
  sf::Color darkred(200,20,40);
  sf::Color lightgrey(240,240,240);
  name.setColor(sel ? darkred : lightgrey);
}

void MenuChoice::setText(std::string newtitle) {
  name.setString(newtitle);
  name.setPosition(1024/2-name.getLocalBounds().width/2,ypos);
}

void MenuChoice::activate() {
  if (callme!=NULL)
    callme->menuEvent(*this);
}

void MenuChoice::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(name);
}

Menu::Menu(std::string name, int default_state, Game* g) : g(g), default_state(default_state) {
  position = 0;
  font.loadFromFile("bauerg.ttf");

  logoTexture.loadFromFile("logo.png");
  logo.setTexture(logoTexture);
  logo.setPosition(float(1024/2)-logo.getLocalBounds().width/2+10, 55);

  background.setSize(sf::Vector2f(1024, 768));
  background.setFillColor(sf::Color(30,30,30));
}

void Menu::addItem(std::string name, int state, bool changemenu, MenuChoiceCallBack* call, int id) {
  MenuChoice* m;
  m = new MenuChoice(name, state, 370, 300+choices.size()*80, font, changemenu, call, id);
  choices.push_back(m);
  if (choices.size()==1)
    m->setSelected(1);
}

void Menu::clear() {
  for (MenuChoice* c : choices) {
    delete c;
  }
  choices.clear();
}

void Menu::draw(sf::RenderTarget& rt, sf::RenderStates states) const {
  rt.draw(background);
  rt.draw(logo);
  for(auto t : choices)
    rt.draw(*t);
}

int Menu::run(sf::RenderWindow& rw, sf::Event& event) {
  rw.draw(*this);
  return processEvents(event);
}

int Menu::processEvents(sf::Event& event)
{
  if (event.type == sf::Event::KeyPressed)  {
    switch (event.key.code)
    {
      case sf::Keyboard::Return:
        prev_key_event=0;
        if(position < choices.size()) {
          choices[position]->activate();
          if (choices[position]->getChangeMenu())
            return choices[position]->getNextState();
        }
        return default_state;

      case sf::Keyboard::Up:
        if (prev_key_event==sf::Keyboard::Up)
          break;
        prev_key_event=sf::Keyboard::Up;
        if(position > 0) {
          choices[position]->setSelected(false);
          position--;
          choices[position]->setSelected(true);
        }

        return default_state;
      case sf::Keyboard::Down:
        if (prev_key_event==sf::Keyboard::Down)
          break;
        prev_key_event=sf::Keyboard::Down;
        if(position < choices.size() - 1) {
          choices[position]->setSelected(false);
          position++;
          choices[position]->setSelected(true);
        }
        return default_state;
      default:
        prev_key_event=0; 
    } 
  } else {
    prev_key_event=0;
  }
  return default_state;
}

