#include "../include/MapObjectBoost.hpp"
#include <sstream>

MapObjectBoost::MapObjectBoost(b2World* world, std::istringstream& s) : MapObjectCollectable(world, s, MAPOBJ_IDENT_BOOST) {
    init();
}

MapObjectBoost::MapObjectBoost(b2World* world, float x, float y) : MapObjectCollectable(world, x,y, MAPOBJ_IDENT_BOOST) {
  init();
}

void MapObjectBoost::init() {
  myShape.m_radius = 1.0f;
  boost = B2toSFRenderer::CircleToSFCircle(myShape);
  boosttexture.loadFromFile("boost.png");
  boosttexture.setSmooth(true);
  boost.setTexture(&boosttexture, true);
  boost.setOrigin(boost.getRadius(), boost.getRadius());
  boost.setPosition(x*20*2,-y*20*2);
  addToWorld();
}

std::string MapObjectBoost::getName() const { return "boost"; }
  
void MapObjectBoost::draw(sf::RenderTarget &rt, sf::RenderStates states) const
{
  if(!collected) {rt.draw(boost);}
}

b2Shape& MapObjectBoost::getShape() {
  return myShape;  
}
