#include "../include/TurboTruck.hpp"

TurboTruck::TurboTruck(b2World* world) : TwoWheelVehicle(world)
{
  /* Parameters */
  {
    maxSpeed = 25.0f;
    maxMotorSpeed = 20.0f;
    angularImpulse = 140.0f;
    boostFactor = 1.7;
    acceleration = 10.0f;
    driveType = RearWheelDrive;
  }

  /* Chassis */
  {
    chassisTexturePath = "TurboTruck_body.png";

    chassis_distance = 2.8f;
    chassisVertices[0].Set(-4.0f, -0.4f);
    chassisVertices[1].Set(4.0f, -0.4f);
    chassisVertices[2].Set(4.0f, 0.9f);
    chassisVertices[3].Set(1.5f, 1.8f);
    chassisVertices[4].Set(-1.5f, 1.8f);
    chassisVertices[5].Set(-4.0f, 0.9f);

    // Material
    chassisFixture.density = 0.4f;
    chassisFixture.friction = 0.5f;
    chassisFixture.restitution = 0.1;
  }

  /* Wheels */
  {
    wheelTexturePath = "tire.png";
    rearWheelRadius = 1.5f;
    frontWheelRadius = 1.5f;

    rearWheel_position.Set(-2.4f, 1.3f);
    frontWheel_position.Set(2.5f, 1.3f);

    // Motors (joints)

    rearWheelJoint.maxMotorTorque = 350.0f;
    rearWheelJoint.frequencyHz = 3;
    rearWheelJoint.dampingRatio = 0.4;

    frontWheelJoint.maxMotorTorque = 350.0f;
    frontWheelJoint.frequencyHz = 3;
    frontWheelJoint.dampingRatio = 0.4;

    // Materials

    rearWheelFixture.density = 0.6f;
    rearWheelFixture.friction = 1.8f;
    rearWheelFixture.restitution = 0.2f;

    frontWheelFixture.density = 0.6f;
    frontWheelFixture.friction = 1.8f;
    frontWheelFixture.restitution = 0.2f;
  }

  // Initialization !important
  initialize();
}
