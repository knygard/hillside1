#include "../include/MapObject.hpp"

std::ostream& operator<<(std::ostream& os, const MapObject& b) {
  os << b.getAsText();
  return os;
} 
